<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    protected $fillable = [
        'nameYear'
    ];
    public function trackings()
    {
        return $this->belongsToMany('App\Tracking')
            ->withPivot('accomplishment', 'year_id', 'tracking_id');
    }
}
