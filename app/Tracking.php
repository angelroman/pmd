<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    protected $fillable = [
        'accomplishment', 'observations'
    ];
    public function line()
    {
        return $this->belongsTo('App\Line');
    }
    public function managments()
    {
        return $this->belongsToMany('App\Managment');
    }
    public function secretaries()
    {
        return $this->belongsToMany('App\Secretary');
    }
    public function years()
    {
        return $this->belongsToMany('App\Year')
            ->withPivot('accomplishment', 'year_id', 'tracking_id');
    }
}
