<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Line extends Model
{
    public $timestamps = false;
    //public $incrementing = false;
    protected $fillable = [
        'number', 'name'
    ];
    public function strategy()
    {
        return $this->belongsTo('App\Strategy');
    }
    public function trackings()
    {
        return $this->hasMany('App\Tracking');
    }
}
