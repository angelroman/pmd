<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Secretary extends Model
{
    protected $fillable = [
        'name'
    ];
    public function trackings()
    {
        return $this->belongsToMany('App\Tracking');
    }
}
