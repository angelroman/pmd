<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Responsible extends Model
{
    // No se usa esta tabla
    protected $fillable = [
        'name'
    ];
}
