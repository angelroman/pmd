<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Objective extends Model
{
    public $timestamps = false;
    //public $incrementing = false;
    protected $fillable = [
        'number', 'name'
    ];
    public function axis()
    {
        return $this->belongsTo('App\Axis');
    }
    public function strategies()
    {
        return $this->hasMany('App\Strategy');
    }
}
