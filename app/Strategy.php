<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Strategy extends Model
{
    public $timestamps = false;
    //public $incrementing = false;
    protected $fillable = [
        'number', 'name'
    ];
    public function objective()
    {
        return $this->belongsTo('App\Objective');
    }
    public function lines()
    {
        return $this->hasMany('App\Line');
    }
}
