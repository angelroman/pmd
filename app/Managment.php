<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Managment extends Model
{
    protected $fillable = [
        'name'
    ];

    public function trackings()
    {
        return $this->belongsToMany('App\Tracking');
    }
}
