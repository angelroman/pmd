<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Axis extends Model
{
    public $timestamps = false;
    //public $incrementing = false;
    protected $fillable = [
        'number', 'name'
    ];
    public function objectives()
    {
        return $this->hasMany('App\Objective');
    }
}
