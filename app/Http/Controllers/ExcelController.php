<?php

namespace App\Http\Controllers;

use App\Managment;
use App\Secretary;
use App\Tracking;
use App\Year;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Axis;
use App\Objective;
use App\Line;

use App\Http\Requests;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    protected $Axes = array();
    protected $Objectives = array();
    protected $Strategies = array();
    protected $Lines = array();

    public function getAxes() {
        Excel::load('/data/files/EvalPMD.xlsx', function($reader) {
            $resuls = $reader->get();
            $axe = array("id" => null, "name" => null);
            foreach($resuls[1] as $_eje) {
                $axe["number"] = $_eje->no;
                $axe["name"] = $_eje->eje;
                array_push($this->Axes, $axe);
            }
            echo json_encode($this->Axes, JSON_UNESCAPED_UNICODE);
        });
    }
    public function getObjectives() {
        Excel::load('/data/files/EvalPMD.xlsx', function($reader) {
            $resuls = $reader->get();
            $porciones = null;
            $newNum = null;
            $objective = array("id" => null, "name" => null);
            foreach($resuls[2] as $_objective) {
                $porciones = explode(".", trim($_objective->no));
                $newNum = $porciones[0].".".$porciones[1];
                $objective["number"] = $newNum;
                $objective["name"] = $_objective->objetivo;
                array_push($this->Objectives, $objective);
                $objective = array("id" => null, "name" => null);
            }
            echo json_encode($this->Objectives, JSON_UNESCAPED_UNICODE);
        });
    }
    public function getStrategies() {
        Excel::load('/data/files/EvalPMD.xlsx', function($reader) {
            $resuls = $reader->get();
            $porciones = null;
            $newNum = null;
            $strategy = array("id" => null, "name" => null);
            foreach($resuls[3] as $_strategy) {
                $porciones = explode(".", trim($_strategy->no));
                $newNum = $porciones[0].".".$porciones[1].".".$porciones[2];
                $strategy["number"] = $newNum;
                $strategy["name"] = $_strategy->estrategia;
                array_push($this->Strategies, $strategy);
                $strategy = array("id" => null, "name" => null);
            }
            echo json_encode($this->Strategies, JSON_UNESCAPED_UNICODE);
        });
    }
    public function getLines() {
        Excel::load('/data/files/EvalPMD.xlsx', function($reader) {
            $resuls = $reader->get();
            $porciones = null;
            $newNum = null;
            $line = array("id" => null, "name" => null);
            foreach($resuls[4] as $_line) {
                $porciones = explode(".", trim($_line->no));
                $newNum = $porciones[0].".".$porciones[1].".".$porciones[2].".".$porciones[3];
                $line["number"] = $newNum;
                $line["name"] = $_line->linea_de_accion;
                array_push($this->Lines, $line);
                $line = array("id" => null, "name" => null);
            }
            echo json_encode($this->Lines, JSON_UNESCAPED_UNICODE);
        });
    }
    public $excel = [];
    public function getCumplimientos() {
        Excel::load('/resources/files/EvalPMD.xlsx', function($reader) use (&$excel) {
            $objExcel = $reader->getExcel();
            $sheet = $objExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow(); //374
            $highestColumn = $sheet->getHighestColumn(); //P

            /*
             * 0 => Eje 1
             * 1 => Objetivo 1.1.
             * 2 => Estrategia 1.1.1.
             * 3 => ID (Linea) 1.1.1.1.
             * 4 => Linea de accion (TEXT)
             * 5 =>
             * 6 => Cumplimiento (N, P, S, N)
             * 7 => SEC (SOCIAL, OBRAS, ADMON, AYU ..)
             * 8 => DIR (ZOO, PROY, OBRAS ....)
             * 9 => COORDINACION NO
             * 10 =>JEFATURA NO
             * 11 => 2014 (S, N, S/N, P, X)
             * 12 => 2015 (S, N, S/N, P, X)
             * 13 => observaciones (TEXT  NULL)
             * 14 => responsable (SAD - Direccion de Informatica - Ing. Victor Flores )
            */


            $arrLine = null;
            $numberLine= null;
            $Line = null;
            $cumplimiento = null;
            $secretaria = null;
            $direccion = null;
            $_2014 = null;
            $_2015 = null;
            $observaciones = null;
            $responsable = null;

            $eje = null;
            $excel = array();
            $Secretarias = array();
            $arrSecretaries = null;
            $arrDirecciones = null;
            $Direcciones = array();
            $Seguimientos = array();

            for ($row = 4; $row <= 371; $row++)
            {
                $cell = $sheet->rangeToArray('B' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $arrLine = explode(".", trim($cell[0][3]));
                $cumplimiento = trim($cell[0][6]);
                $secretaria = trim($cell[0][7]);
                $direccion = trim($cell[0][8]);
                $arrSecretaries = explode("/",$secretaria);
                $arrDirecciones = explode("/", $direccion);
                for($i = 0; $i < count($arrSecretaries); $i++) {
                    array_push($Secretarias, trim($arrSecretaries[$i]));
                }
                for($i = 0; $i < count($arrDirecciones); $i++) {
                    array_push($Direcciones, trim($arrDirecciones[$i]));
                }
                $_2014 = trim($cell[0][11]);
                $_2015 = trim($cell[0][12]);
                $observaciones = trim($cell[0][13]);
                $responsable = trim($cell[0][14]);
                $numberLine= "$arrLine[0].$arrLine[1].$arrLine[2].$arrLine[3]";
                //$eje = Axis::where('number', $numberLine)->first();
                $Line = Line::where('number', $numberLine)->first();
                array_push($Seguimientos, array(
                    "line_id" => $Line->id,
                    "number_line" => $Line->number,
                    "cumplimiento" => $cumplimiento,
                    "Secretarias" => $Secretarias,
                    "Direcciones" => $Direcciones,
                    "_2014" => $_2014,
                    "_2015" => $_2015,
                    "observaciones" => $observaciones,
                    "responsable" => $responsable
                ));
                $arrSecretaries = null;
                $Secretarias = array();
                $arrDirecciones = null;
                $Direcciones = array();
            }
            //dd($Seguimientos);
            echo json_encode($Seguimientos, JSON_UNESCAPED_UNICODE);
            //$Line = Line::where('number', $number)->first();
            //$Line->trackings();
            //$eje = Axis::where('number', '1')->first();
        });
    }
    public function getResponsibles() {
        /*Excel::load('./data/files/EvalPMD.xlsx', function($reader) {
            $objExcel = $reader->getExcel();
            $sheet = $objExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow(); //374
            $highestColumn = $sheet->getHighestColumn(); //P
            for ($row = 4; $row <= 371; $row++)
            {
                $cell = $sheet->rangeToArray('B' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $responsable = trim($cell[0][14]);
            }
        });*/
        //$json = File::get("database/data/Trackings.json");

        $json = File::get("../database/data/Trackings.json");
        $Trackings = json_decode($json);
        $t = null;
        $y = Year::where('nameYear', '2014')->first();
        /*foreach ($Trackings as $track) {
            $track->_2014 =  strtoupper($track->_2014);
            $t = Tracking::where('line_id', $track->line_id)->first();
            $t->years()->attach($t->id, [
                'accomplishment' => $track->_2014,
                'year_id' => $y->id
            ]);
        }
        $y = Year::where('nameYear', '2015')->first();
        foreach ($Trackings as $track) {
            $track->_2015 =  strtoupper($track->_2015);
            $t = Tracking::where('line_id', $track->line_id)->first();
            $t->years()->attach($t->id, [
                'accomplishment' => $track->_2015,
                'year_id' => $y->id
            ]);
        }*/
    }
    public function Secretarias() {
        /*$json = File::get("../database/data/Trackings.json");
        $Trackings = json_decode($json);
        $Secretarias = array();
        $secretaria = null;
        $t = null;
        foreach($Trackings as $tracking) {
            $t = Tracking::where('line_id', $tracking->line_id)->first();
            for($i = 0; $i < count($tracking->Secretarias); $i++) {
                $secretaria = Secretary::where('name', $tracking->Secretarias[$i])->first();
                echo $secretaria->id . "<br/>";
                $t->secretaries()->attach($secretaria->id);
            }
            echo "<br/><br/>";
        }*/
    }

    public function Direcciones() {
        $json = File::get("../database/data/Trackings.json");
        $Trackings = json_decode($json);
        //dd($Trackings);
        $direccion = null;
        $t = null;
        foreach($Trackings as $tracking) {
            $t = Tracking::where('line_id', $tracking->line_id)->first();
            for($i = 0; $i < count($tracking->Direcciones); $i++) {
                $direccion = Managment::where('name', $tracking->Direcciones[$i])->first();
                echo $direccion->name . "<br/>";
                //$t->managments()->attach($direccion);
            }
            echo "<br/><br/>";
        }
    }
}