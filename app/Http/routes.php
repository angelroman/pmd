<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/Axes', 'ExcelController@getAxes');
Route::get('/Objectives', 'ExcelController@getObjectives');
Route::get('/Strategies', 'ExcelController@getStrategies');
Route::get('/Lines', 'ExcelController@getLines');
Route::get('/Cumplimiento', 'ExcelController@getCumplimientos');
Route::get('/Responsables', 'ExcelController@getResponsibles');
Route::get('/Secretarias', 'ExcelController@Secretarias');
Route::get('/Direcciones', 'ExcelController@Direcciones');

/*Route::get('login', [
    'uses' => 'Auth\AuthController@getLogin',
    'as' => 'login'
]);*/

// Authentication routes...
/*Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', ['as' => 'auth/register', 'uses' => 'Auth\AuthController@postRegister']);*/


Route::auth();
Route::get('/home', 'HomeController@index');

Route::get('/line/{number}', 'LineController@getLine');