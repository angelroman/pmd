class Strategy {
    constructor(strategy) {
        this.Lines = [];
        if (typeof strategy != 'undefined') {
            this.number = strategy.number;
            this.name = strategy.name;
            this.getLines();
        }
        else {
            this.number = null;
            this.name = null;
        }
        if(this.name == null || this.name == undefined) {
            this.getName();
        }
    }
    getLines() {
        var _line = null;
        var number = null;
        let line = null;
        for(var i = 0; i < Lines.length; i++) {
            _line = Lines[i].number.split(".");
            number = _line[0]+"."+_line[1]+"."+_line[2];
            if(this.number == number) {
                line = new Line(Lines[i]);
                this.Lines.push(line);
                number = null;
                _line = null;
                line = null;
            }
        }
    }
    getName() {
        for(var i = 0; i < Strategies.length; i++) {
            if(Strategies[i].number == this.number) {
                this.name = Strategies[i].name;
                return;
            }
        }
    }
    openDialogLines() {
        $("#modalLines").openModal();
        $('#modalLines .modal-content .collection').empty();
        var li = null;
        var self = this;
        console.log(self);
        $('#title-content-line').empty().append("<h3>"+self.name+"</h3>");
        for(var i = 0; i < this.Lines.length; i++) {
            li = "<li id='"+this.Lines[i].number+"' class=\"collection-item avatar\" onclick='ShowCumplimiento(this.id);'>" +
                "<div class='container-line valign-wrapper'>" +
                    "<i class=\"material-icons circle blue darken-2 fa fa-check fa-2x valign\"></i>" +
                    "<h5>"+this.Lines[i].name+"</h5>" +
                    //"<a href=\"#!\" class=\"secondary-content\"><i class=\"fa fa-hourglass-start\"></i></a>" +
                "</div>" +
            "</li>";
            $('#modalLines .modal-content .collection').append(li);
        }
    }
    getListLines(id) {
        var list = [];
        var li = null;
        for(var i = 0; i < this.Lines.length; i++) {
            li = "<li id='"+this.Lines[i].number+"' class=\"collection-item avatar\" onclick='ShowCumplimiento(this.id);'>" +
                "<div class='container-line valign-wrapper'>" +
                "<i class=\"material-icons circle blue darken-2 fa fa-check fa-2x valign\"></i>" +
                "<h5>"+this.Lines[i].name+"</h5>" +
                //"<a href=\"#!\" class=\"secondary-content\"><i class=\"fa fa-hourglass-start\"></i></a>" +
                "</div>" +
                "</li>";
            list.push(li);
        }
        return list;
    }
}