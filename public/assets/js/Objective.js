class Objective {
    constructor(objective) {
        this.Strategies = [];
        if (typeof objective != 'undefined') {
            this.number = objective.number;
            this.name = objective.name;
            this.getStrategies();
        }
        else {
            this.number = null;
            this.name = null;
        }
    }
    getStrategies() {
        var _strategy = null;
        var number = null;
        let strategy = null;
        for(var i = 0; i < Strategies.length; i++) {
            _strategy = Strategies[i].number.split(".");
            number = _strategy[0]+"."+_strategy[1];
            if(this.number == number) {
                strategy = new Strategy(Strategies[i]);
                this.Strategies.push(strategy);
                _strategy = null;
                number = null;
                strategy = null;
            }
        }
    }
}