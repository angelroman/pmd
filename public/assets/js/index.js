$(document).ready(function () {
    let usuario = new User();
    var tarjetas = $(".tarjeta");
    var tarjeta1 = new Waypoint({
        element: ($(tarjetas)[0]),
        handler: function(direction) {
            usuario.CountCard(0, 6, 0, 650, function () {
                usuario.CountCard(0, 36, 2, 1000, function () {
                    usuario.CountCard(0, 118, 1, 1000, function () {
                        usuario.CountCard(0, 368, 3, 800, function () {
                            return;
                        })
                    });
                });
            });
        },
        offset: '70%'
    });
    //$('.fixed-action-btn').openFAB(function () {});
    /*var c = 0;
    $("#btnShowBar").click(function () {
        debugger;
        if(c == 0) {
            $("#fixed-container").css('display', 'block').removeClass('animated fadeOut').addClass("animated fadeIn");
            c = 1;
        }
        else {
            $("#fixed-container").removeClass("animated fadeIn").addClass('animated fadeOut').css('display', 'none');
            c = 0;
        }

    });*/
    $(".arrow-down").click(function() {
        $(this).fadeOut('fast');
        $('body, html').animate({
            scrollTop: $("#eje1").offset().top
        }, 300);
    });
    $('.arrow-up').click(function(){
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
        $($(".arrow-down")[0]).fadeIn('fast');
    });

    $(window).scroll(function(){
        if( $(this).scrollTop() > 0 ){
            $('.arrow-up').slideDown(300);
            $(".arrow-down").removeClass('infinite pulse').addClass('zoomOut');
        } else {
            $('.arrow-up').slideUp(300);
            $(".arrow-down").removeClass('zoomOut').addClass('infinite pulse');
        }
    });

    /*var presentation = new Waypoint({
        element: document.getElementById('container-page'),
        handler: function(direction) {
            usuario.CountCard(0, 6, 0, 650, function () {
                usuario.CountCard(0, 36, 2, 1000, function () {
                    usuario.CountCard(0, 118, 1, 1000, function () {
                        usuario.CountCard(0, 368, 3, 800, function () {
                            return;
                        })
                    });
                });
            });
        },
        offset: '70%'
    });*/

    $(".fixed-element").mouseenter(function () {
        var id = this.id.split("-");
        var axis = usuario.getAxis(id[1]);
        $("#"+this.id + " h5").fadeOut("slow", function () {
            $(this).empty();
            $(this).addClass("animated slideInLeft").append(axis.name).fadeIn("slow");
        });
    });
    $(".fixed-element").mouseleave(function () {
        var id = this.id.split("-");
        $("#"+this.id + " h5").empty().append(usuario.getNumberRoman(id[1]));
    });
    $(".fixed-element").click(function () {
        var id = this.id.split("-");
        console.log(id[1]);
        $('html,body').animate({
            scrollTop: $("#eje"+id[1]).offset().top}, 2000);
    });

    //$('.scrollspy').scrollSpy();
    /*
    var options = [
        {
            selector: '#staggered-test', offset: 50, callback: function(el) {
            Materialize.toast("This is our ScrollFire Demo!", 1500 );
            }
        },
        {
            selector: '#staggered-test', offset: 205, callback: function(el) {
                Materialize.toast("Please continue scrolling!", 1500 );
            }
        },
        {
            selector: '#staggered-test', offset: 400, callback: function(el) {
                Materialize.showStaggeredList($(el));
            }
        },
        {
            selector: '#image-test', offset: 500, callback: function(el) {
                Materialize.fadeInImage($(el));
            }
        }
    ];
    Materialize.scrollFire(options);*/
    /*$($timer1).countTo({
        from: 0,
        to: 6,
        speed: 1000,
        refreshInterval: 50,
        formatter: function (value, options) {
            //return value.toFixed(options.decimals);
        },
        onUpdate: function (value) {
            //console.debug(this);
        },
        onComplete: function (value) {
            //console.debug(this);
        }
    });*/



    var waypoint1 = new Waypoint({
        element: document.getElementById('eje1'),
        handler: function(direction) {
            usuario.ShowEje(1);
            //notify(this.element.id + ' triggers at ' + this.triggerPoint)
        },
        offset: '70%'
    });
    var waypoint2 = new Waypoint({
        element: document.getElementById('eje2'),
        handler: function(direction) {
            usuario.ShowEje(2);
            //notify(this.element.id + ' triggers at ' + this.triggerPoint)
        },
        offset: '70%'
    });
    var waypoint3 = new Waypoint({
        element: document.getElementById('eje3'),
        handler: function(direction) {
            usuario.ShowEje(3);
            //notify(this.element.id + ' triggers at ' + this.triggerPoint)
        },
        offset: '70%'
    });
    var waypoint4 = new Waypoint({
        element: document.getElementById('eje4'),
        handler: function(direction) {
            usuario.ShowEje(4);
            //notify(this.element.id + ' triggers at ' + this.triggerPoint)
        },
        offset: '70%'
    });
    var waypoint5 = new Waypoint({
        element: document.getElementById('eje5'),
        handler: function(direction) {
            usuario.ShowEje(5);
            //notify(this.element.id + ' triggers at ' + this.triggerPoint)
        },
        offset: '70%'
    });
    var waypoint6 = new Waypoint({
        element: document.getElementById('eje6'),
        handler: function(direction) {
            usuario.ShowEje(6);
            //notify(this.element.id + ' triggers at ' + this.triggerPoint)
        },
        offset: '70%'
    });

});



function ShowListStrategies(li) {
    var id = li.id;
    let axis = new Axis(id.split('.')[0]);
    axis.ShowStrategies(id);
    $("#eje"+id.split('.')[0]+" .ul-objetivos li").removeClass('li-objetivo-activo');
    $(li).addClass('li-objetivo-activo');
}
function ShowDialogLines (id) {
    let strategy = new Strategy({ number: id });
    strategy.openDialogLines();
}
function ShowCumplimiento(id) {
    $("#modalLines").closeModal();
    var _id = id.split('.');
    var line_id = _id[0] + "." + _id[1] + "." + _id[2];
    let dialog = new Dialog("dialogAccomplishment");
    let line = new Line({ number: id });
    var linea = line.getLine();
    console.log(linea.tracking.years);
    dialog.title = linea.name;
    dialog.close = function () {
        ShowDialogLines(line_id);
    };
    var ulLineYears = $("#dialogAccomplishment .dialog-content #line-years ul.collection")[0];
    $(ulLineYears).empty();
    for(var i = 0; linea.tracking.years.length; i++) {
        $(ulLineYears).append("<li class=\"collection-item avatar\">" +
            "<i class=\"material-icons circle red\"></i>" +
            "<span class=\"title\">"+linea.tracking.years[i]+"</span>" +
            "<a href=\"#!\" class=\"secondary-content\"><i class=\"material-icons\"></i></a>" +
            "</li>");
    }
    dialog.openDialog();
}