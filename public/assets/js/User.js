class User {
    Login(email, password) {
        var frmLogin = $("#frmLogin");
        var formData = frmLogin.serialize();
        $.ajax({
            url: 'auth/login',
            type: "POST",
            data: formData,
            success: function (response) {
                console.log(response);
            },
            error: function (response) {
                console.log(response);
            }
        });
    }
    getAxis(id) {
        let axis = new Axis(id);
        return axis;
    }
    ShowEje(id) {
        let axis = new Axis(id);
        axis.ShowTitle();
        axis.ShowObjectives();
        axis.ShowStrategies(axis.Objectives[0].number);
    }
    getNumberRoman(number) {
        switch (number) {
            case '1':
                return 'I';
                break;
            case '2':
                return 'II';
                break;
            case '3':
                return 'III';
                break;
            case '4':
                return 'IV';
                break;
            case '5':
                return 'V';
                break;
            case '6':
                return 'VI';
                break;
        }
    }
    CountCard(start, end, element, speed, callback) {
        //speed = 100
        var $timer = $('.timerCard')[element];
        $($timer).countTo({
            from: start,
            to: end,
            speed: speed,
            refreshInterval: 50,
            formatter: function (value, options) {
                return value.toFixed(options.decimals);
            },
            onUpdate: function (value) {
                //console.debug(this);
            },
            onComplete: function (value) {
                //console.debug(this);
                callback();
            }
        });
    }
}
