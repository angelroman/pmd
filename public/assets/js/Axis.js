class Axis {
    constructor(id) {
        this.Objectives = [];
        if (typeof id != 'undefined') {
            this.getAxis(id);
            this.getObjectives();
        }
        else {
            this.number = null;
            this.name = null;
        }
    }
    getAxis(id) {
        for(var i = 0; i < Axes.length; i++) {
            if(Axes[i].number == id) {
                this.number = Axes[i].number;
                this.name = Axes[i].name;
                this.getObjectives();
                return;
            }
        }
    }
    getObjectives() {
        var _objective = null;
        let objective = null;
        for(var i = 0; i < Objectives.length; i++) {
            _objective = Objectives[i].number.split(".");
            if(this.number == _objective[0]) {
                objective = new Objective(Objectives[i]);
                this.Objectives.push(objective);
                objective = null;
                _objective = null;
            }
        }
    }
    ShowTitle() {
        $("#eje"+this.number+" .objetivos").css("display", 'inline-block');
        $("#eje"+this.number+" .titulo").empty().append("<h2 class='animated fadeInLeftBig'>"+this.name+"</h2>");
    }
    ShowObjectives() {
        var self = this;
        $("#eje"+this.number+" .title-objective").css("display", 'block');
        $("#eje"+this.number+" .title-objective").empty().append("<h3 class=\"animated fadeInLeftBig\">Objetivos</h3>");


        $("#eje"+this.number+" .ul-objetivos").empty().css("display", "block").addClass("animated fadeInLeftBig");
        let objective = null;
        for(var i = 0; i < this.Objectives.length; i++) {
            objective = new Objective(this.Objectives[i]);
            if(i == 0) {
                $("#eje"+this.number+" .ul-objetivos").append("<li class=\"li-objetivo-activo animated fadeInLeft\" id=\""+objective.number+"\" onclick=\"ShowListStrategies(this);\">"+objective.name+"</li>");
            }
            else {
                $("#eje"+this.number+" .ul-objetivos").append("<li class=\"animated fadeInLeft\" id=\""+objective.number+"\" onclick=\"ShowListStrategies(this);\">"+objective.name+"</li>");
            }
            objective = null;
        }
    }
    ShowStrategies(idObjective) {
        console.log(idObjective);
        $("#eje"+this.number+" .title-strategy").css("display", 'block');
        $("#eje"+this.number+" .title-strategy").empty().append("<h3 class=\"animated fadeInLeftBig\">Estrategias</h3>");

        $("#eje"+this.number+" .estrategias").css("display", "inline-block");
        $("#eje"+this.number+" .ul-estrategias").empty().css("display", "block").addClass("animated fadeInLeftBig");
        var i = 0;
        var Strategies = [];
        for(i = 0; i < this.Objectives.length; i++) {
            if(this.Objectives[i].number == idObjective) {
                Strategies = this.Objectives[i].Strategies;
            }
        }
        for(i = 0; i < Strategies.length; i++) {
            $("#eje"+this.number+" .ul-estrategias").append("<li class=\"animated fadeInLeft\" id=\""+Strategies[i].number+"\" onclick=\"ShowDialogLines(this.id);\"><i class=\"fa fa-minus\" aria-hidden=\"true\"></i>     "+Strategies[i].name+"</li>");
        }
    }
}
