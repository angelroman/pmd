class Line {
    constructor(line) {
        if (typeof line != 'undefined') {
            this.number = line.number;
            this.name = line.name;
        }
        else {
            this.number = null;
            this.name = null;
        }
        this.id = null;
        this.tracking = {};
        this.strategy_id = null;
    }
    getLine() {
        var self = this;
        var line = {};
        $.ajax({
            url: "/line/"+self.number,
            type: "GET",
            dataType: "JSON",
            async: false,
            success: function (response) {
                console.log(response);
                line = response.line;
                line.tracking = response.tracking;
                //self.tracking = response.tracking;
            },
            error: function (xhr) {
                console.log(xhr);
            }
        });
        console.log(self);
        return line;
    }
    getListYears() {
        var list = [];
        for(var i = 0; this.tracking.years; i++) {
            list.push("<li class=\"collection-item avatar\">" +
                "<i class=\"material-icons circle red\"></i>" +
                "<span class=\"title\">"+this.tracking.years[i].nameYear+"</span>" +
                "<a href=\"#!\" class=\"secondary-content\"><i class=\"material-icons\"></i></a>" +
            "</li>");
        }
        return list;
    }
}