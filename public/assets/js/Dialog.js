class Dialog {
    constructor(id, z_index = 3000) {
        var self = this;
        this.open = false;
        this.id = id;
        this.backGround = $("#"+this.id+" .dialog-background");
        this.wrapper = $("#"+this.id+" .dialog-wrapper");
        this.dialog = $("#"+this.id+" .dialog");
        this.dialogTitle = $("#dialogAccomplishment .dialog-title");
        this.title = null;
        this.z_index = z_index;
        $("#"+id).css("z-index", self.z_index);
        this.backGround.css("z-index", self.z_index);
        this.wrapper.css("z-index", self.z_index);
        this.close = null;
        this.opening = null;
        this.body = null;
        //var body = $("<div class=\"animated dialog-background\"></div><div class=\"animated dialog-wrapper\"><div class=\"animated dialog\"><div class=\"dialog-btn-close circle\"><i class=\"fa fa-times\"></i></div><div class=\"dialog-content\"></div></div></div>");
        $("#"+this.id+" .dialog-btn-close").click(function () {
            self.closeDialog();
        });
        if (typeof fn != 'undefined') {
            this.fn = fn;
        }
    }
    openDialog() {
        var self = this;
        if(this.open == false) {
            this.backGround.css("display", "block").removeClass("zoomOut").addClass("zoomIn");
            this.wrapper.css("display", "block").removeClass("zoomOut").addClass("zoomIn");
            this.dialog.removeClass("zoomOut").addClass("zoomIn");
            if (this.opening !== null) {
                // Debe ser una funcion, se ejecuta cuando se abre
                this.dialogTitle.empty().append(self.title);
                this.opening();
            }
            this.open = true;
        }
    }
    closeDialog() {
        if(this.open == true) {
           var self = this;
            this.backGround.removeClass("zoomIn").addClass("zoomOut");
            this.wrapper.removeClass("zoomIn").addClass("zoomOut");
            this.dialog.removeClass("zoomIn").addClass("zoomOut");
            setTimeout(function () {
                self.backGround.css("display", "none");
                self.wrapper.css("display", "none");
            }, 700);
            if (this.close !== null) {
                this.close();
            }
            this.open = false;
        }
    }
}