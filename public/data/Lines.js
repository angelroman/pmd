var Lines = [{
    "number": "1.1.1.1",
    "name": "Acatando lo dispuesto en la Ley de Transparencia, dotar de información necesaria en el sitio."
}, {
    "number": "1.1.2.1",
    "name": "Consulta electrónica de saldo del impuesto predial."
}, {
    "number": "1.1.2.2",
    "name": "Implementar pago electrónico."
}, {
    "number": "1.1.2.3",
    "name": "Registro de solicitudes de atención ciudadana en el sitio web."
}, {
    "number": "1.1.3.1",
    "name": "Cumplir con el Artículo 16 de la Ley de Transparencia y Acceso a la Información Pública del Estado de Tamaulipas."
}, {
    "number": "1.1.3.2",
    "name": "Cumplir con el Artículo 42 de la Ley de Transparencia y Acceso a la Información Pública del Estado de Tamaulipas."
}, {
    "number": "1.1.4.1",
    "name": "Mapa digital de avance de obras."
}, {
    "number": "1.1.4.2",
    "name": "Mapa digital de actividades realizadas por la Secretaría de Servicios Públicos Primarios."
}, {
    "number": "1.1.4.3",
    "name": "Trazo de rutas de vialidad por obras en proceso."
}, {
    "number": "1.1.4.4",
    "name": "Desarrollo de aplicaciones móviles."
}, {
    "number": "1.1.5.1",
    "name": "Definir las funciones de las dependencias municipales."
}, {
    "number": "1.1.5.2",
    "name": "Actualización de manuales de procedimientos."
}, {
    "number": "1.1.5.3",
    "name": "Elaborar descripciones de puestos."
}, {
    "number": "1.1.5.4",
    "name": "Integrar expediente laboral de servidores públicos."
}, {
    "number": "1.1.5.5",
    "name": "Elaborar tabuladores de rangos de sueldos."
}, {
    "number": "1.1.6.1",
    "name": "Definir misión, visión y valores."
}, {
    "number": "1.1.6.2",
    "name": "Elaborar manual de identidad gubernamental."
}, {
    "number": "1.1.7.1",
    "name": "Mapeo de procesos."
}, {
    "number": "1.1.7.2",
    "name": "Definición de objetivos."
}, {
    "number": "1.1.7.3",
    "name": "Estadística de seguimiento de objetivos alcanzados."
}, {
    "number": "1.1.8.1",
    "name": "Bitácora de mantenimiento para las unidades."
}, {
    "number": "1.1.8.2",
    "name": "Plan de reacondicionamiento mecánico."
}, {
    "number": "1.1.8.3",
    "name": "Compra de unidades nuevas para las Secretarías prioritarias."
}, {
    "number": "1.1.9.1",
    "name": "Convocatoria e inscripción al padrón de proveedores."
}, {
    "number": "1.1.9.2",
    "name": "Análisis estadístico de Adquisiciones."
}, {
    "number": "1.2.1.1",
    "name": "Instaurar los lineamientos de los consejos de colaboración vecinal."
}, {
    "number": "1.2.1.2",
    "name": "Determinar responsabilidades, alcances y titulares de los consejos de colaboración vecinal."
}, {
    "number": "1.2.2.1",
    "name": "Incorporar a las Organizaciones de la Sociedad Civil (OSC)."
}, {
    "number": "1.2.2.2",
    "name": "Administración de proyectos de coinversión de las OSC."
}, {
    "number": "1.2.2.3",
    "name": "Integrar el consejo general vecinal."
}, {
    "number": "1.2.3.1",
    "name": "Creación del Instituto Municipal de la Juventud, donde se fomentará la participación, atención y desarrollo de los jóvenes. "
}, {
    "number": "1.2.4.1",
    "name": "Centro Integral de Atención Ciudadana (CIAC), en un centro se recibirán todas la peticiones de la Ciudadanía para su seguimiento. "
}, {
    "number": "1.2.4.2",
    "name": "Línea Directa, programa donde el Presidente y su gabinete ampliado, visitan una colonia y recaban distintas peticiones de la ciudadanía."
}, {
    "number": "1.3.1.1",
    "name": "Programa de transparencia en el gasto público y el uso de recursos."
}, {
    "number": "1.3.1.2",
    "name": "Establecer la metodología y medios donde se den a conocer los resultados de este programa."
}, {
    "number": "1.3.2.1",
    "name": "Establecer la agenda semestral para el análisis y evaluación del desempeño administrativo y la gestión del servicio público."
}, {
    "number": "1.3.2.2",
    "name": "Se contratará una empresa externa especializada para el análisis imparcial."
}, {
    "number": "1.3.2.3",
    "name": "Evaluación externa de la gestión Municipal, a través del Programa “Agenda para el Desarrollo Municipal”. "
}, {
    "number": "1.3.3.1",
    "name": "Presentar cada doce meses el nivel de calificación crediticia otorgado por las empresas especialistas del ramo."
}, {
    "number": "1.4.1.1",
    "name": "Adecuarse a la nueva armonización contable."
}, {
    "number": "1.4.1.2",
    "name": "Ajustarse a las nuevas reglas de operación contables gubernamental."
}, {
    "number": "1.4.2.1",
    "name": "Optimizar y adecuarse a los nuevos sistemas, manteniendo un equilibrio presupuestal racional y austero, privilegiando el gasto social."
}, {
    "number": "1.4.3.1",
    "name": "Catastro eficiente, eficaz elaboración de manifiestos y tasas justas, para que la mayoría de los predios se ajusten a la normatividad vigente."
}, {
    "number": "1.4.3.2",
    "name": "Recaudación eficiente del impuesto predial."
}, {
    "number": "1.4.3.3",
    "name": "Maximizar el cobro de impuestos, derechos y aprovechamientos, de una manera eficiente y amable hacia el contribuyente."
}, {
    "number": "1.4.4.1",
    "name": "Actualización y control del patrimonio inmobiliario."
}, {
    "number": "1.4.4.2",
    "name": "Actualización de los inventarios, en cuanto a características, valor y personas responsables."
}, {
    "number": "1.4.4.3",
    "name": "Actualización de la información del parque vehicular municipal."
}, {
    "number": "1.4.5.1",
    "name": "Estricto seguimiento de las notificaciones municipales."
}, {
    "number": "1.4.5.2",
    "name": "Seguimiento a las multas no fiscales."
}, {
    "number": "2.1.1.1",
    "name": "Convenios de colaboración para el patrullaje disuasivo del delito con el Estado y la Federación."
}, {
    "number": "2.1.1.2",
    "name": "Participar en las reuniones quincenales del Comité de Seguridad."
}, {
    "number": "2.1.1.3",
    "name": "Vinculación con la Plataforma México. "
}, {
    "number": "2.1.2.1",
    "name": "Seguimiento del Programa de prevención del delito y conducta antisocial (PECAS)."
}, {
    "number": "2.1.2.2",
    "name": "Desarrollo y mejora de la Academia de Seguridad."
}, {
    "number": "2.1.2.3",
    "name": "Análisis Estadístico del Delito Cometido."
}, {
    "number": "2.1.3.1",
    "name": "Gestionar recursos ante el SUBSEMUN para su aplicación en la ciudad."
}, {
    "number": "2.1.3.2",
    "name": "Compra de equipo de vigilancia y monitoreo con tecnología de punta."
}, {
    "number": "2.2.1.1",
    "name": "Cursos en escuelas para educación vial."
}, {
    "number": "2.2.1.2",
    "name": "Cursos en Centros laborales de Cultura Vial."
}, {
    "number": "2.2.1.3",
    "name": "Programa del Adulto Mayor, donde personas de edad avanzada apoyan como Agentes Auxiliares."
}, {
    "number": "2.2.2.1",
    "name": "Compra de uniformes y chamarras para los Agentes."
}, {
    "number": "2.2.2.2",
    "name": "Modernización del parque vehicular asignado al departamento de tránsito."
}, {
    "number": "2.2.2.3",
    "name": "Reacondicionar la instalaciones del departamento de tránsito."
}, {
    "number": "2.2.3.1",
    "name": "Campaña de “Deschatarrización”, consiste en retirar de la vía pública los autos no aptos para circular."
}, {
    "number": "2.2.3.2",
    "name": "Campaña “Ponte Listo”, consiste en reiterar a los automovilistas y pasajeros el uso del cinturón de seguridad."
}, {
    "number": "2.2.3.3",
    "name": "Campaña “Operación Alcoholímetro”, consiste en aplicar exámenes de alcoholemia en choferes de vehículos."
}, {
    "number": "2.2.3.4",
    "name": "Campaña “Si tomas No manejes”, busca concientizar al ciudadano a no manejar en estado de ebriedad. "
}, {
    "number": "2.2.3.5",
    "name": "Campaña “Conductor Designado”, busca que los ciudadanos designen un chofer que se haga responsable del vehículo para evitar accidentes por causa del alcohol."
}, {
    "number": "2.2.3.6",
    "name": "Campaña “No Texteo”, busca que los automovilistas no se distraigan mandando mensajes en sus dispositivos móviles mientras conducen."
}, {
    "number": "2.2.3.7",
    "name": "Campaña “Anticorrupción”, con el fin de denunciar a los agentes viales que soliciten dinero a cambio de no aplicar infracciones. "
}, {
    "number": "2.3.1.1",
    "name": "Adecuaciones de Instalaciones y Mobiliario."
}, {
    "number": "2.3.1.2",
    "name": "Renovación del parque vehicular y equipamiento. "
}, {
    "number": "2.3.2.1",
    "name": "Constante realización de simulacros, para estar preparados ante cualquier contingencia."
}, {
    "number": "2.3.2.2",
    "name": "Apoyos a las distintas Secretarías en eventos programados."
}, {
    "number": "2.3.2.3",
    "name": "Auditoría a empresas particulares en cumplimiento de normas de Protección Civil."
}, {
    "number": "2.3.2.4",
    "name": "Manejo de incidentes de fuego, accidentes automovilísticos de alto impacto, derrame de sustancias peligrosas y contingencias naturales. "
}, {
    "number": "2.3.3.1",
    "name": "Campaña de concientización de factores de riesgo en las escuelas."
}, {
    "number": "2.3.3.2",
    "name": "Campaña de difusión de protección civil."
}, {
    "number": "2.3.3.3",
    "name": "Constantes capacitaciones al personal de protección civil y bomberos."
}, {
    "number": "2.3.3.4",
    "name": "Realizar cursos de capacitación para las distintas empresas de la localidad. "
}, {
    "number": "2.4.1.1",
    "name": "Implementación del programa de “Cultura de Paz” en la sociedad neolaredense."
}, {
    "number": "2.4.1.2",
    "name": "Programa de prevención de conductas violentas intrafamiliares, de género y entornos escolares."
}, {
    "number": "2.4.1.3",
    "name": "Programa de prevención de violencia en centros comunitarios, educativos y laborales."
}, {
    "number": "2.4.1.4",
    "name": "Programa integral de atención a víctimas de violencia intrafamiliar, acoso escolar y violencia en el noviazgo."
}, {
    "number": "2.4.2.1",
    "name": "Impartir cursos de capacitación de prevención de adicciones."
}, {
    "number": "2.4.2.2",
    "name": "Prevención y atención de las adicciones en adolescentes y jóvenes."
}, {
    "number": "2.4.2.3",
    "name": "Promover el arte, la cultura y el deporte en los adolescentes y jóvenes para la integración en la comunidad. "
}, {
    "number": "2.4.3.1",
    "name": "Diseñar campaña de prevención de accidentes viales."
}, {
    "number": "2.4.3.2",
    "name": "Curso de capacitación para jóvenes estudiantes."
}, {
    "number": "3.1.1.1",
    "name": "Profesionales de economía, comercio exterior, industrial y ventas para la promoción de la ciudad. "
}, {
    "number": "3.1.1.2",
    "name": "Desarrollar presentación electrónica y de video con testimoniales para promoción."
}, {
    "number": "3.1.1.3",
    "name": "Implementar agenda de ferias, congresos y exposiciones."
}, {
    "number": "3.2.1.1",
    "name": "Determinar la ubicación de la Ventanilla Única de Atención al Inversionista."
}, {
    "number": "3.2.1.2",
    "name": "Definir lineamientos interdepartamentales para la Ventanilla Única de Atención al Inversionista."
}, {
    "number": "3.2.1.3",
    "name": "Establecer una agenda con las dependencias municipales relacionadas con la apertura de empresas."
}, {
    "number": "3.2.1.4",
    "name": "Realizar encuesta de salida al inversionista sobre la calidad de la atención. "
}, {
    "number": "3.3.1.1",
    "name": "Desarrollo de parque industrial en Reservas Territoriales."
}, {
    "number": "3.3.1.2",
    "name": "Apoyo a las industrias en el mantenimiento de los servicios públicos como bacheo, alumbrado y señalización. "
}, {
    "number": "3.3.2.1",
    "name": "Apoyo para la rehabilitación del Mercado Maclovio Herrera."
}, {
    "number": "3.3.2.2",
    "name": "Apoyo en la reactivación del Centro Histórico."
}, {
    "number": "3.3.2.3",
    "name": "Mantenimiento de los servicios públicos como bacheo, alumbrado, señalización en las avenidas principales."
}, {
    "number": "3.3.2.4",
    "name": "Promover exposiciones de comercio local."
}, {
    "number": "3.3.3.1",
    "name": "Buscar la reubicación de la estación cuarentenaria, a una ubicación cerca del Puente Internacional III. "
}, {
    "number": "3.3.3.2",
    "name": "Modernización del rastro municipal, para incrementar su competitividad."
}, {
    "number": "3.3.3.3",
    "name": "Promover la comercialización de los productos cárnicos, en comercios de la localidad."
}, {
    "number": "3.3.3.4",
    "name": "Encalichamiento de brechas y caminos rurales."
}, {
    "number": "3.3.3.5",
    "name": "Desarrollo de proyectos agroindustriales de invernadero."
}, {
    "number": "3.3.3.6",
    "name": "Vinculación Agropecuaria con los Programas de la SAGARPA."
}, {
    "number": "3.4.1.1",
    "name": "Activación del proyecto Aeropuerto de Carga."
}, {
    "number": "3.4.1.2",
    "name": "Mejoras en la infraestructura del Puente III."
}, {
    "number": "3.4.1.3",
    "name": "Coadyuvar en las mejoras de las instalaciones ferroviarias."
}, {
    "number": "3.4.2.1",
    "name": "Impulso al Puente IV-V."
}, {
    "number": "3.4.2.2",
    "name": "Adecuación de la Garita del Puente II “Juárez-Lincoln”."
}, {
    "number": "3.4.2.3",
    "name": "Conclusión de la Planta Potabilizadora Norponiente."
}, {
    "number": "3.4.2.4",
    "name": "Gestión para la instalación del Sistema de Aproximación Automática (ILS) en el Aeropuerto."
}, {
    "number": "3.4.3.1",
    "name": "Proyectos de investigación para incrementar la competitividad del Municipio."
}, {
    "number": "3.4.3.2",
    "name": "Análisis cuantitativo y cualitativo del comercio internacional."
}, {
    "number": "3.4.3.3",
    "name": "Actualización anual de diagnósticos socioeconómicos del Municipio y la región."
}, {
    "number": "3.4.4.1",
    "name": "Conformado por representantes de las fuerzas productivas del sector económico de la ciudad."
}, {
    "number": "3.4.4.2",
    "name": "El objetivo primordial del consejo es priorizar obras a realizarse a corto, mediano y largo plazo, para impulsar el turismo, generar empleos, desarrollar y mantener el liderazgo que tiene Nuevo Laredo en materia de transporte y comercio exterior."
}, {
    "number": "3.5.1.1",
    "name": "Desarrollarlo en Centros Comunitarios y Tamules."
}, {
    "number": "3.5.1.2",
    "name": "Desarrollar planes de capacitación permanentes."
}, {
    "number": "3.5.1.3",
    "name": "Trabajar en coordinación con el Sistema Nacional de Empleo (SNE) con el programa “Autoempleo”."
}, {
    "number": "3.5.2.1",
    "name": "Buscar becas de capacitación laboral."
}, {
    "number": "3.5.2.2",
    "name": "Gestionar incentivos económicos durante la capacitación."
}, {
    "number": "3.6.1.1",
    "name": "Gestión de recursos del programa federal “Fomento a Jóvenes Emprendedores”."
}, {
    "number": "3.6.1.2",
    "name": "Gestión de Recursos de Programas del Instituto Nacional de Emprendedor (INADEM)."
}, {
    "number": "3.6.2.1",
    "name": "Establecer el lugar físico."
}, {
    "number": "3.6.2.2",
    "name": "Definir los lineamientos, titulares y responsabilidades."
}, {
    "number": "3.6.3.1",
    "name": "Encuentro con emprendedores para el crecimiento y la consolidación del modelo de franquicias. "
}, {
    "number": "3.7.1.1",
    "name": "Se realizaran 2 ferias del empleo al año en coordinación con el Servicio Estatal del Empleo."
}, {
    "number": "3.7.2.1",
    "name": "Desarrollo de una base de datos de empresas de la localidad con vacantes disponibles."
}, {
    "number": "3.7.2.2",
    "name": "Programa itinerante “Empleo Móvil” en diversos sectores de la ciudad."
}, {
    "number": "3.7.2.3",
    "name": "Vinculación con solicitantes hacia las vacantes previamente identificadas."
}, {
    "number": "3.7.2.4",
    "name": "Desarrollo de programa de software que tenga la Bolsa de Trabajo en línea."
}, {
    "number": "3.8.1.1",
    "name": "Programa “Vive Nuevo Laredo”."
}, {
    "number": "3.8.1.2",
    "name": "Evento Rocky Ruedas, Zoqueteadas."
}, {
    "number": "3.8.2.1",
    "name": "Evento organizado por la CANACO “Catrina”."
}, {
    "number": "3.8.2.2",
    "name": "Evento “Moto Fest”, reunión internacional de motociclistas."
}, {
    "number": "3.8.2.3",
    "name": "Evento de Cabalgatas Regionales."
}, {
    "number": "3.8.2.4",
    "name": "Corridas de Toros."
}, {
    "number": "3.9.1.1",
    "name": "Congreso de ANGADI, CAAAREM y Tianguis Turísticos."
}, {
    "number": "3.9.1.2",
    "name": "Asistencia a foros, ferias y festivales."
}, {
    "number": "3.9.2.1",
    "name": "Hunter Extravaganza, es un Congreso de Cazadores Organizado en Texas."
}, {
    "number": "3.9.2.2",
    "name": "Safari Club Monterrey, es un Congreso Internacional de caza Deportiva."
}, {
    "number": "3.9.2.3",
    "name": "Clúster Médico, es una agrupación que integra todos los servicios médicos de la localidad. "
}, {
    "number": "3.9.2.4",
    "name": "TurisPET, es el desarrollo de una propuesta de turismo médico veterinario enfocado a la atención de mascotas."
}, {
    "number": "3.9.3.1",
    "name": "Feria del Taco, Mitotes, Fiestas Turísticas."
}, {
    "number": "3.9.3.2",
    "name": "Muestra Gastronómica."
}, {
    "number": "3.9.3.3",
    "name": "Concurso Nacional de Cultura Turística."
}, {
    "number": "3.9.3.4",
    "name": "Aniversario de la Ciudad."
}, {
    "number": "3.10.1.1",
    "name": "Talleres de habilidades para el liderazgo general."
}, {
    "number": "3.10.1.2",
    "name": "Marketing Turístico."
}, {
    "number": "3.10.1.3",
    "name": "Inglés técnico para el sector turístico."
}, {
    "number": "3.10.2.1",
    "name": "Instalación de módulos digitales de información turística."
}, {
    "number": "3.10.2.2",
    "name": "Campaña “De este Laredo es más barato”."
}, {
    "number": "3.10.2.3",
    "name": "Campaña de Cupones de Descuentos. "
}, {
    "number": "4.1.1.1",
    "name": "El Instituto Municipal de Investigación, Planeación y Desarrollo Urbano (IMPLADU), elaborará el Plan Municipal de Ordenamiento Territorial y Desarrollo Urbano."
}, {
    "number": "4.1.1.2",
    "name": "Optimizar la inversión pública en equipamiento urbano."
}, {
    "number": "4.1.1.3",
    "name": "Revisar la regularización de usos de suelo. "
}, {
    "number": "4.1.2.1",
    "name": "Reacondicionar la infraestructura actual del Centro Histórico."
}, {
    "number": "4.1.2.2",
    "name": "Reactivar la restauración del patrimonio arquitectónico de Centro Histórico."
}, {
    "number": "4.1.2.3",
    "name": "Elaboración del Reglamento del Centro Histórico."
}, {
    "number": "4.1.3.1",
    "name": "El IMPLADU desarrollará estudios de flujos vehiculares y movilidad."
}, {
    "number": "4.1.3.2",
    "name": "Crear el plan de reordenamiento vial en base a los estudios de flujo."
}, {
    "number": "4.1.3.3",
    "name": "Integrar las acciones de reordenamiento vial al programa de obra pública."
}, {
    "number": "4.1.4.1",
    "name": "Establecer las normas para ordenar y regular los fraccionamientos."
}, {
    "number": "4.1.4.2",
    "name": "Estructurar las infracciones, sanciones, recursos de inconformidad y procedimientos administrativos para su aplicación."
}, {
    "number": "4.1.4.3",
    "name": "El Instituto Municipal de Vivienda y Suelo Urbano (IMVISU), continuará con los programas de vivienda y convenios con el INFONAVIT y constructoras privadas. "
}, {
    "number": "4.1.5.1",
    "name": "Establecer las normas para garantizar las condiciones habitables y de seguridad estructural de las edificaciones. "
}, {
    "number": "4.1.5.2",
    "name": "Definir los procesos de ejecución, modificación o demolición de obras públicas o privadas."
}, {
    "number": "4.1.5.3",
    "name": "Definir las reglas para usos de construcciones o edificaciones públicas y privadas."
}, {
    "number": "4.1.5.4",
    "name": "Establecer los procedimientos para la obtención de permisos para construir, remodelar o demoler cualquier tipo de construcción. "
}, {
    "number": "4.1.5.5",
    "name": "Estructurar las infracciones y sanciones así como procedimientos administrativos que permitan su aplicación. "
}, {
    "number": "4.2.1.1",
    "name": "Primaria en la Colonia Villas de San Miguel."
}, {
    "number": "4.2.1.2",
    "name": "Primaria en la Colonia Reservas Territoriales."
}, {
    "number": "4.2.1.3",
    "name": "Construcción de Desayunadores. "
}, {
    "number": "4.2.1.4",
    "name": "Construcción de Aulas de Apoyo."
}, {
    "number": "4.2.2.1",
    "name": "Construcción de Secundarias."
}, {
    "number": "4.2.2.2",
    "name": "Construcción de un “Campus de Educación medio y medios superior”"
}, {
    "number": "4.2.3.1",
    "name": "Bardas perimetrales, pintura, banquetas, instalaciones eléctricas, impermeabilizaciones, rehabilitaciones a módulos sanitarios y canchas deportivas. "
}, {
    "number": "4.3.1.1",
    "name": "Construcción de Centro de Desarrollo Comunitario “La Concordia”."
}, {
    "number": "4.3.1.2",
    "name": "Construcción de Centro Juvenil “Palmares” y “Silao”."
}, {
    "number": "4.3.1.3",
    "name": "Construcción de Bibliotecas e Infotecas. "
}, {
    "number": "4.3.2.1",
    "name": "Rehabilitación de los 5 TAMULES de la ciudad."
}, {
    "number": "4.3.2.2",
    "name": "Rehabilitación de centros comunitarios."
}, {
    "number": "4.3.2.3",
    "name": "Remozamiento de la Casa de la Cultura y Teatro de la ciudad."
}, {
    "number": "4.3.2.4",
    "name": "Remozamiento Centro Cultural."
}, {
    "number": "4.4.1.1",
    "name": "Unidades Deportivas."
}, {
    "number": "4.4.1.2",
    "name": "Campos de Béisbol “Blvd. Colosio”."
}, {
    "number": "4.4.1.3",
    "name": "Área deportiva Colonia “Américo Villarreal”."
}, {
    "number": "4.4.1.4",
    "name": "Canchas de Fut-Rap."
}, {
    "number": "4.4.1.5",
    "name": "Gimnasio Escuadrón 201."
}, {
    "number": "4.4.2.1",
    "name": "Parque “La Raqueta” en Colonia Los Aztecas."
}, {
    "number": "4.4.2.2",
    "name": "Gimnasios de Box."
}, {
    "number": "4.4.2.3",
    "name": "Campos deportivos."
}, {
    "number": "4.5.1.1",
    "name": "20 Plazas de “Pies Mojados”."
}, {
    "number": "4.5.1.2",
    "name": "Desarrollo de Parques lineales."
}, {
    "number": "4.5.1.3",
    "name": "Desarrollo de Macro Parques."
}, {
    "number": "4.5.2.1",
    "name": "Fuentes."
}, {
    "number": "4.5.2.2",
    "name": "Monumentos. "
}, {
    "number": "4.5.2.3",
    "name": "Plazas Públicas."
}, {
    "number": "4.6.1.1",
    "name": "Construcción del Centro de Atención de Salud Mental."
}, {
    "number": "4.6.1.2",
    "name": "Construcción del Servicio Médico Forense (SEMEFO)."
}, {
    "number": "4.6.1.3",
    "name": "Rehabilitación de Dispensarios Médicos."
}, {
    "number": "4.6.1.4",
    "name": "Proyecto de Farmacias Municipales. "
}, {
    "number": "4.6.2.1",
    "name": "Rehabilitación de la Estación de Bomberos y las tres subestaciones."
}, {
    "number": "4.6.2.2",
    "name": "Construcción del Centro de Acopio de Residuos Peligrosos."
}, {
    "number": "4.6.2.3",
    "name": "Construcción del Centro de Acopio de Llantas de desecho."
}, {
    "number": "4.6.3.1",
    "name": "Desarrollo del proyecto “Mall del Yonke”."
}, {
    "number": "4.6.3.2",
    "name": "Edificio de Dependencias Municipales. "
}, {
    "number": "4.6.3.3",
    "name": "Unidad especializada de atención de víctimas de género (UNEVIG)."
}, {
    "number": "4.6.3.4",
    "name": "Edificación del complejo para el Instituto Municipal de Prevención de Adicciones, Conductas y tendencias Antisociales (IMPACTA). "
}, {
    "number": "4.7.1.1",
    "name": "Av. Reforma."
}, {
    "number": "4.7.1.2",
    "name": "Av. Monterrey y Anzures."
}, {
    "number": "4.7.1.3",
    "name": "Av. Tecnológico."
}, {
    "number": "4.7.1.4",
    "name": "Av. Río Pánuco."
}, {
    "number": "4.7.1.5",
    "name": "Av. Emiliano Zapata."
}, {
    "number": "4.7.1.6",
    "name": "Ampliación Carretera al Aeropuerto."
}, {
    "number": "4.7.1.7",
    "name": "Ampliación Carretera Mex-II."
}, {
    "number": "4.7.1.8",
    "name": "Av. Eva Sámano."
}, {
    "number": "4.7.2.1",
    "name": "Pavimentación de 1,000 calles."
}, {
    "number": "4.7.2.2",
    "name": "Recarpeteo y reciclado de calles."
}, {
    "number": "4.7.2.3",
    "name": "Rehabilitación de colectores."
}, {
    "number": "4.7.2.4",
    "name": "Construcción de puentes vehiculares."
}, {
    "number": "4.7.2.5",
    "name": "Construcción de puentes peatonales."
}, {
    "number": "4.7.2.6",
    "name": "Continuar con la construcción de los drenajes pluviales."
}, {
    "number": "4.7.2.7",
    "name": "Contratos de infraestructura emergente en alumbrado público, agua potable y drenaje."
}, {
    "number": "4.8.1.1",
    "name": "Establecimiento de la Dirección de Mantenimiento de Edificios Públicos."
}, {
    "number": "4.8.1.2",
    "name": "Creación de Talleres de Semaforización y Señalización."
}, {
    "number": "4.8.1.3",
    "name": "Desarrollo de la Oficina de Inspección Urbana."
}, {
    "number": "4.8.1.4",
    "name": "Implementar el área de Calidad y Capacitación."
}, {
    "number": "4.8.2.1",
    "name": "Programa de abatimiento del rezago de alumbrado público."
}, {
    "number": "4.8.2.2",
    "name": "Implementación de alumbrado con lámparas LED."
}, {
    "number": "4.8.2.3",
    "name": "Proyecto inventario de lámparas de alumbrado público, para Georreferenciación, por tipo, modelo y sector."
}, {
    "number": "4.8.3.1",
    "name": "Campaña intensiva de bacheo."
}, {
    "number": "4.8.3.2",
    "name": "Programa de bacheo en avenidas principales."
}, {
    "number": "4.8.3.3",
    "name": "Atención al reporte ciudadano."
}, {
    "number": "4.8.4.1",
    "name": "Programa de limpieza en avenidas principales."
}, {
    "number": "4.8.4.2",
    "name": "Supervisión del contrato con SETASA."
}, {
    "number": "4.8.4.3",
    "name": "Mantenimiento de arroyos y canales."
}, {
    "number": "4.8.4.4",
    "name": "Limpieza de lotes baldíos."
}, {
    "number": "4.8.4.5",
    "name": "Saneamiento de basureros clandestinos."
}, {
    "number": "4.8.5.1",
    "name": "Rehabilitación del sistema de drenaje sanitario."
}, {
    "number": "4.8.5.2",
    "name": "Ampliación de la red de agua potable."
}, {
    "number": "4.8.5.3",
    "name": "Continuación de la desconexión de algunos puntos del drenaje sanitario del drenaje pluvial. "
}, {
    "number": "4.8.5.4",
    "name": "Programa uso eficiente del agua."
}, {
    "number": "4.8.5.5",
    "name": "Fomentar la sustentabilidad de la planta tratadora de aguas. "
}, {
    "number": "4.8.6.1",
    "name": "Ampliación del panteón Municipal II."
}, {
    "number": "4.8.6.2",
    "name": "Adecuación del Rastro Municipal."
}, {
    "number": "4.8.6.3",
    "name": "Rehabilitación de “El Laguito”; Saneamiento biológico integral y desarrollo del área comercial."
}, {
    "number": "4.8.6.4",
    "name": "Desarrollo del Parque Viveros II."
}, {
    "number": "5.1.1.1",
    "name": "Ejecución de proyectos técnicos viables. "
}, {
    "number": "5.1.2.1",
    "name": "Desarrollo de un programa de limpieza constante."
}, {
    "number": "5.1.3.1",
    "name": "Reforestación en espacios públicos."
}, {
    "number": "5.1.3.2",
    "name": "Proyecto Parque Viveros II."
}, {
    "number": "5.1.3.3",
    "name": "Reforestación de centros educativos."
}, {
    "number": "5.2.1.1",
    "name": "Campaña de difusión de Cultura Ambiental."
}, {
    "number": "5.2.1.2",
    "name": "Campaña permanente de limpieza."
}, {
    "number": "5.2.1.3",
    "name": "Implementación del barrido mecánico."
}, {
    "number": "5.3.1.1",
    "name": "Campaña de pláticas ecológicas en los centros educativos, industrias, comercios y a la sociedad civil."
}, {
    "number": "5.3.1.2",
    "name": "Programa de recolección de residuos peligrosos y electrónicos."
}, {
    "number": "5.3.1.3",
    "name": "Proyecto “Casa de la Tierra” en coinversión con el gobierno estatal, es un sistema de simulación esférica con información audiovisual de fenómenos climáticos."
}, {
    "number": "5.3.1.4",
    "name": "Censo de vulcanizadoras con plan de recolección de llantas. "
}, {
    "number": "5.3.1.5",
    "name": "Fortalecer el programa de recolección, confinamiento temporal y trituración de neumáticos, así como promover investigaciones para disponer de ellos."
}, {
    "number": "5.3.1.6",
    "name": "Seguimiento puntual del programa de monitoreo atmosférico."
}, {
    "number": "5.3.1.7",
    "name": "Reúso del agua residual tratada. "
}, {
    "number": "5.3.2.1",
    "name": "Desarrollo de programas de inspección y vigilancia. "
}, {
    "number": "5.3.2.2",
    "name": "Reactivar convenio de disposición final de neumáticos usados."
}, {
    "number": "5.3.2.3",
    "name": "Proponer una disminución del uso y manejo de las bolsas de plástico."
}, {
    "number": "6.1.1.1",
    "name": "Fortalecer la infraestructura y equipamiento de planteles educativos en el municipio."
}, {
    "number": "6.1.2.1",
    "name": "Otorgar becas a estudiantes de bajos recursos, en edad escolar consistentes en apoyos económicos y en especie."
}, {
    "number": "6.1.2.2",
    "name": "Otorgar becas de excelencia a estudiantes con altos promedios de calificación de todos los niveles académicos."
}, {
    "number": "6.1.2.3",
    "name": "Entregar de forma gratuita uniformes a todos los estudiantes de nivel básico."
}, {
    "number": "6.1.2.4",
    "name": "Entregar de forma gratuita útiles escolares a todos los estudiantes de preescolar, primaria y secundaria."
}, {
    "number": "6.1.2.5",
    "name": "Se premiará a los alumnos con más alto promedio por salón de clases con una tableta electrónica."
}, {
    "number": "6.1.3.1",
    "name": "Contratación de personal docente eventual para cubrir el déficit en las escuelas públicas. "
}, {
    "number": "6.1.3.2",
    "name": "Contratación de personal de vigilancia para cubrir el déficit en las escuelas públicas de nivel básico. "
}, {
    "number": "6.1.3.3",
    "name": "Programa Integral de Apoyo Educativo y Emocional."
}, {
    "number": "6.1.4.1",
    "name": "Fortalecimiento de las Bibliotecas Públicas Municipales existentes, así como la creación de dos adicionales."
}, {
    "number": "6.1.4.2",
    "name": "Seguimiento de programas deportivos, tecnológicos, ambientales y cívicos impartidos en las distintas escuelas."
}, {
    "number": "6.1.4.3",
    "name": "Certamen Municipal “Juventud Innovadora”."
}, {
    "number": "6.1.4.4",
    "name": "Conmemoración del Día del Maestro y Homenaje por Trayectoria. "
}, {
    "number": "6.1.4.5",
    "name": "Programa de Trasporte escolar municipal. "
}, {
    "number": "6.2.1.1",
    "name": "Apoyos y servicios que procuren resolver situaciones de emergencia familiar."
}, {
    "number": "6.2.1.2",
    "name": "Despensa básica, apoyo en especie a las familias en situaciones vulnerables. "
}, {
    "number": "6.2.2.1",
    "name": "Fortalecimiento de los valores familiares a través de la organización de eventos comunitarios. "
}, {
    "number": "6.2.2.2",
    "name": "Integración de Comités Vecinales con la finalidad de establecer corresponsabilidad entre Gobierno y Sociedad para reconstruir el tejido social."
}, {
    "number": "6.2.2.3",
    "name": "Desarrollo de un Programa de Estadística Social para la Optimización de los Beneficios Sociales."
}, {
    "number": "6.2.3.1",
    "name": "Implementación y seguimiento del Programa FISM: Fomento de Infraestructura Social Municipal."
}, {
    "number": "6.2.3.2",
    "name": "Implementar el Programa Hábitat en el desarrollo de habilidades y destrezas físicas mediante talleres de oficios."
}, {
    "number": "6.2.3.3",
    "name": "Mejorar la calidad de vida de los ciudadanos mediante el Programa “Rescate de Espacios Públicos”, en donde se vincula el desarrollo urbano con el desarrollo social. "
}, {
    "number": "6.2.3.4",
    "name": "Programa de Empleo Temporal."
}, {
    "number": "6.2.4.1",
    "name": "Ampliación y adecuación de los cinco Tamules “Lugar de Encuentro”, donde se impartirán diferentes talleres y programas recreativos."
}, {
    "number": "6.2.4.2",
    "name": "Adecuación de los tres Centros Comunitarios donde se impartirán talleres culturales, programas deportivos y cursos computacionales."
}, {
    "number": "6.2.4.3",
    "name": "Instalación de un albergue temporal para atención de personas en situaciones de calle y vulnerabilidad social. "
}, {
    "number": "6.2.5.1",
    "name": "Tiene como misión fundamental el promover acciones integrales de asistencia social en las áreas de salud, jurídica, de educación, integración familiar y de nutrición."
}, {
    "number": "6.2.5.2",
    "name": "Las acciones de este Organismo están encaminadas a la protección de los grupos vulnerables de nuestra sociedad."
}, {
    "number": "6.2.5.3",
    "name": "Colaboración y coordinación con Organismos Federales, Estatales y Organizaciones de la Sociedad Civil (OSC)."
}, {
    "number": "6.3.1.1",
    "name": "Organización, promoción y difusión del Festival Internacional Tamaulipas (FIT)"
}, {
    "number": "6.3.1.2",
    "name": "Organización de la Muestra Internacional de Cine."
}, {
    "number": "6.3.1.3",
    "name": "Organización de diversos festivales: de Jazz, Monólogos X Edición, Infantiles y de Arte Joven."
}, {
    "number": "6.3.2.1",
    "name": "Organización de talleres de iniciación y creación artística."
}, {
    "number": "6.3.2.2",
    "name": "Desarrollo del taller de artes escénicas."
}, {
    "number": "6.3.2.3",
    "name": "Instauración del taller de restauración de muebles y pintura textil."
}, {
    "number": "6.3.3.1",
    "name": "Organización de visitas guiadas a los distintos museos de la localidad."
}, {
    "number": "6.3.3.2",
    "name": "Programa de Club de Lectura del Centro “Estación Palabra”."
}, {
    "number": "6.3.3.3",
    "name": "Control de la agenda de los espacios culturales."
}, {
    "number": "6.3.3.4",
    "name": "Programa “Vive el Centro Cívico”, donde se realizará una muestra mensual."
}, {
    "number": "6.3.4.1",
    "name": "Organización de exposiciones en los distintos museos de la Ciudad."
}, {
    "number": "6.3.4.2",
    "name": "Organización de exposiciones en el Centro Histórico dentro del Programa “Vive Nuevo Laredo”."
}, {
    "number": "6.3.5.1",
    "name": "Realización de las semanas culturales y “Conciertos para Todos”."
}, {
    "number": "6.3.5.2",
    "name": "Organización cultural del “Rock Fest”."
}, {
    "number": "6.3.5.3",
    "name": "Celebración de los 50 años del Teatro de la Ciudad."
}, {
    "number": "6.3.5.4",
    "name": "Apoyo en la organización de eventos especiales."
}, {
    "number": "6.3.6.1",
    "name": "Organización del cine club, jueves artísticos de plaza y el GAM (Grupos Artísticos Municipales)."
}, {
    "number": "6.3.6.2",
    "name": "Fortalecimiento de la Escuela de Música y el Programa de producción musical."
}, {
    "number": "6.3.6.3",
    "name": "Promover a nuestra compañía de danza."
}, {
    "number": "6.3.6.4",
    "name": "Estimular a la Centenaria Banda de música Municipal a participar y promover la cultura musical en la ciudadanía."
}, {
    "number": "6.3.7.1",
    "name": "Activación del Instituto de Cultura, con el fin de promover la cultura en todas sus modalidades en la ciudad."
}, {
    "number": "6.4.1.1",
    "name": "Fomento de inicialización deportiva donde se contemplan 5 disciplinas básicas, con entrenamientos diarios y en coparticipación con el Gobierno Estatal. "
}, {
    "number": "6.4.1.2",
    "name": "Academias deportivas municipales donde fomenta la práctica diaria de 15 disciplinas."
}, {
    "number": "6.4.1.3",
    "name": "Organización de torneos deportivos de las diferentes disciplinas."
}, {
    "number": "6.4.1.4",
    "name": "Hacia la profesionalización deportiva, instrucción a entrenadores sobre nutrición, medicina deportiva y evaluación del rendimiento del Atleta. "
}, {
    "number": "6.4.2.1",
    "name": "Festival deportivo en tu colonia."
}, {
    "number": "6.4.2.2",
    "name": "Realizar “Campamentos de Verano” de 7 distintas disciplinas."
}, {
    "number": "6.4.2.3",
    "name": "Realizar torneos deportivos en fechas conmemorativas."
}, {
    "number": "6.4.2.4",
    "name": "Organización de carreras atléticas urbanas. "
}, {
    "number": "6.4.2.5",
    "name": "Plan de “Circuito Deportivo”, visita en escuelas para detección de talentos."
}, {
    "number": "6.4.2.6",
    "name": "Jugando con tus estrellas. "
}, {
    "number": "6.4.3.1",
    "name": "Promoción a la capacitación interna, externa y clínicas de especialización."
}, {
    "number": "6.4.3.2",
    "name": "Programa “Actívate”, integrado por clases diarias de activación física y un evento masivo al mes."
}, {
    "number": "6.4.3.3",
    "name": "Programa “Ponte al 100”, donde se realizan 22 pruebas de signos vitales, postura, desempeño y condición física."
}, {
    "number": "6.4.3.4",
    "name": "Programa “Cuerpo sano, Vida sana”, consiste en estudio médico, asesoría nutricional y rutinas deportivas. "
}, {
    "number": "6.4.4.1",
    "name": "Becas a la excelencia deportiva."
}, {
    "number": "6.4.4.2",
    "name": "Apoyos en logística de transporte para competiciones foráneas."
}, {
    "number": "6.4.4.3",
    "name": "Instrumentar apoyos en material deportivo."
}, {
    "number": "6.4.4.4",
    "name": "Incorporar las necesidades de afiliación a la federación de las disciplinas deportivas. "
}, {
    "number": "6.4.5.1",
    "name": "Creación del Instituto de Cultura Física y Deporte, el cual desarrollará programas permanentes de promoción y difusión al deporte. "
}, {
    "number": "6.5.1.1",
    "name": "Atención médica a Jubilados y Pensionados."
}, {
    "number": "6.5.1.2",
    "name": "Atención y seguimiento médico de empleados."
}, {
    "number": "6.5.1.3",
    "name": "Campañas de fomento a la salud dentro del Gobierno Municipal."
}, {
    "number": "6.6.1.1",
    "name": "Implementación de políticas de igualdad en las dependencias municipales."
}, {
    "number": "6.6.1.2",
    "name": "Implementación de acciones afirmativas en beneficio de las mujeres dentro de los programas sociales."
}, {
    "number": "6.6.1.3",
    "name": "Impulsar el empoderamiento económico y educativo que les permita a las mujeres incorporase a la población productiva. "
}, {
    "number": "6.6.2.1",
    "name": "Establecer medidas especiales para prevenir, erradicar y sancionar la violencia de género, en coordinación con el Instituto Municipal de la Mujer, las distintas instancias gubernamentales y sociales."
}, {
    "number": "6.6.2.2",
    "name": "Garantizar el cumplimiento de las leyes de protección a la mujer "
}, {
    "number": "6.6.3.1",
    "name": "Fomentar la participación política de las mujeres."
}, {
    "number": "6.6.3.2",
    "name": "Promover la igualdad de oportunidades en el acceso y permanencia laboral."
}, {
    "number": "6.7.1.1",
    "name": "Asignar grupos de trabajo, instalaciones y recursos a este programa."
}, {
    "number": "6.7.1.2",
    "name": "Mejorar las condiciones de vida de los ciudadanos, en los sectores impactados por el PAC."
}, {
    "number": "6.7.2.1",
    "name": "Organizar un PAC por mes con la construcción de un área deportiva y rehabilitación de una plaza; y la atención a los servicios públicos de esa zona."
}, {
    "number": "6.7.2.2",
    "name": "Toma de protesta del Comité Vecinal que vigilará las obras generadas."
}, {
    "number": "6.7.2.3",
    "name": "Programa Viernes Social Mente-Responsable, este programa consiste en restituir el tejido social, con actividades deportivas, culturales, sociales y brigadas médico asistenciales."
}, {
    "number": "6.8.1.1",
    "name": "Participar en congresos, cursos y capacitaciones diversas."
}, {
    "number": "6.8.1.2",
    "name": "Desarrollar actividades culturales en conjunto con el patronato “Amigos del Zoológico”."
}, {
    "number": "6.8.1.3",
    "name": "Desarrollar página web y archivo fotográfico."
}, {
    "number": "6.8.1.4",
    "name": "Dotar de uniformes al personal como parte de la nueva imagen. "
}, {
    "number": "6.8.2.1",
    "name": "Infraestructura y de áreas comunes, como pintura, señalización, vehículos y áreas verdes."
}, {
    "number": "6.8.2.2",
    "name": "Hábitats y jaulas, limpieza y fumigaciones de los distintos hábitats así como mantenimiento de las áreas climatizadas de las diversas especies. "
}, {
    "number": "6.8.3.1",
    "name": "Se contará con un staff veterinario especializado para mantener la salud y el bienestar de nuestras especies."
}, {
    "number": "6.8.3.2",
    "name": "El personal zootecnista establecerá la correcta nutrición y alimentación."
}, {
    "number": "6.8.3.3",
    "name": "Se elaborarán normas y manuales de seguridad para empleados y visitantes. "
}, {
    "number": "6.8.4.1",
    "name": "Ampliación del Zoológico."
}, {
    "number": "6.8.4.2",
    "name": "Programa de intercambio y adquisición de nuevas especies."
}, {
    "number": "6.8.4.3",
    "name": "Adecuación del Herpetario."
}, {
    "number": "6.8.4.4",
    "name": "Casa de noche y manejo de jirafas."
}];