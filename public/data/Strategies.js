var Strategies = [{
  "number": "1.1.1",
  "name": "Rediseño del sitio web del Gobierno Municipal"
}, {
  "number": "1.1.2",
  "name": "Implementar trámites municipales en línea"
}, {
  "number": "1.1.3",
  "name": "Sistema de transparencia y acceso a la información"
}, {
  "number": "1.1.4",
  "name": "Sistema de Georreferenciación Municipal"
}, {
  "number": "1.1.5",
  "name": "Reestructuración del organigrama municipal"
}, {
  "number": "1.1.6",
  "name": "Redefinir la cultura e imagen institucional"
}, {
  "number": "1.1.7",
  "name": "Establecer políticas de calidad"
}, {
  "number": "1.1.8",
  "name": "Optimización del parque vehicular municipal"
}, {
  "number": "1.1.9",
  "name": "Reingeniería de los procesos del departamento de adquisiciones"
}, {
  "number": "1.2.1",
  "name": "Formar los consejos de colaboración vecinal"
}, {
  "number": "1.2.2",
  "name": "Formar el consejo de participación ciudadana"
}, {
  "number": "1.2.3",
  "name": "Instituto Municipal de la Juventud (IMJUVE)"
}, {
  "number": "1.2.4",
  "name": "Programas insignia de la presente administración"
}, {
  "number": "1.3.1",
  "name": "Implementación del Programa “Así Vamos”"
}, {
  "number": "1.3.2",
  "name": "Informe de la gestión municipal"
}, {
  "number": "1.3.3",
  "name": "Estatus de calificación crediticia"
}, {
  "number": "1.4.1",
  "name": "Contabilidad Gubernamental "
}, {
  "number": "1.4.2",
  "name": "Correcta administración de Egresos "
}, {
  "number": "1.4.3",
  "name": "Correcta administración de Ingresos "
}, {
  "number": "1.4.4",
  "name": "Adecuado control del patrimonio municipal "
}, {
  "number": "1.4.5",
  "name": "Ejecución fiscal"
}, {
  "number": "2.1.1",
  "name": "Coordinación con las distintas corporaciones de Seguridad"
}, {
  "number": "2.1.2",
  "name": "Programas de Seguridad"
}, {
  "number": "2.1.3",
  "name": "Inversión en Seguridad"
}, {
  "number": "2.2.1",
  "name": "Cultura de educación vial"
}, {
  "number": "2.2.2",
  "name": "Inversión en el Departamento de tránsito"
}, {
  "number": "2.2.3",
  "name": "Implementación de Campañas"
}, {
  "number": "2.3.1",
  "name": "Adecuaciones de infraestructura "
}, {
  "number": "2.3.2",
  "name": "Operativos"
}, {
  "number": "2.3.3",
  "name": "Capacitación y Difusión"
}, {
  "number": "2.4.1",
  "name": "Prevención de la Violencia "
}, {
  "number": "2.4.2",
  "name": "Prevención de las Adicciones"
}, {
  "number": "2.4.3",
  "name": "Movilidad segura y campañas de seguridad vial "
}, {
  "number": "3.1.1",
  "name": "Conformar equipo profesional"
}, {
  "number": "3.2.1",
  "name": "Creación de la Ventanilla Única "
}, {
  "number": "3.3.1",
  "name": "Industrial"
}, {
  "number": "3.3.2",
  "name": "Comercial"
}, {
  "number": "3.3.3",
  "name": "Desarrollo Rural"
}, {
  "number": "3.4.1",
  "name": "Fortalecimiento para el Comercio Internacional"
}, {
  "number": "3.4.2",
  "name": "Fortalecimiento para la competitividad"
}, {
  "number": "3.4.3",
  "name": "Instituto para la Competitividad y el Comercio Exterior de Nuevo Laredo (ICCE de Nuevo Laredo)"
}, {
  "number": "3.4.4",
  "name": "Integrar el Consejo Rector de Desarrollo Económico"
}, {
  "number": "3.5.1",
  "name": "Programa “Nunca es tarde para empezar”"
}, {
  "number": "3.5.2",
  "name": "Gestionar programas estatales, federales e internacionales de empleo"
}, {
  "number": "3.6.1",
  "name": "Programa municipal de apoyo a MIPYMES"
}, {
  "number": "3.6.2",
  "name": "Implementar programa Incubadora Municipal de Empresas"
}, {
  "number": "3.6.3",
  "name": "Desarrollo del Taller de Emprendedores "
}, {
  "number": "3.7.1",
  "name": "Organización de Ferias del Empleo"
}, {
  "number": "3.7.2",
  "name": "Bolsa de Trabajo"
}, {
  "number": "3.8.1",
  "name": "Apoyo Turístico Institucional"
}, {
  "number": "3.8.2",
  "name": "Apoyo Turístico a Organizaciones"
}, {
  "number": "3.9.1",
  "name": "Congresos a asistir"
}, {
  "number": "3.9.2",
  "name": "Campañas de Promoción "
}, {
  "number": "3.9.3",
  "name": "Eventos Locales"
}, {
  "number": "3.10.1",
  "name": "Programa de Capacitación "
}, {
  "number": "3.10.2",
  "name": "Competitividad turística"
}, {
  "number": "4.1.1",
  "name": "Verificar un Crecimiento Urbano y Sustentable"
}, {
  "number": "4.1.2",
  "name": "Reactivación del Plan del Centro Histórico"
}, {
  "number": "4.1.3",
  "name": "Establecer el Plan de Reordenamiento Vial para un mejor aprovechamiento de la infraestructura"
}, {
  "number": "4.1.4",
  "name": "Implementar un Reglamento Municipal de Fraccionamientos"
}, {
  "number": "4.1.5",
  "name": "Implementar un Reglamento Municipal de Construcciones"
}, {
  "number": "4.2.1",
  "name": "Construcción de Escuelas de Nivel Básico "
}, {
  "number": "4.2.2",
  "name": "Construcción de Escuelas de Nivel Medio y Medio Superior"
}, {
  "number": "4.2.3",
  "name": "Mantenimientos diversos a la infraestructura educativa "
}, {
  "number": "4.3.1",
  "name": "Construcción de centros culturales"
}, {
  "number": "4.3.2",
  "name": "Reparaciones y ampliaciones de centros culturales"
}, {
  "number": "4.4.1",
  "name": "Rehabilitación de centros deportivos"
}, {
  "number": "4.4.2",
  "name": "Construcción de centros deportivos"
}, {
  "number": "4.5.1",
  "name": "Construcción de Plazas y Parques"
}, {
  "number": "4.5.2",
  "name": "Remozamiento de infraestructura recreativa"
}, {
  "number": "4.6.1",
  "name": "Salud"
}, {
  "number": "4.6.2",
  "name": "Protección Ciudadana"
}, {
  "number": "4.6.3",
  "name": "Servicios a la Ciudadanía"
}, {
  "number": "4.7.1",
  "name": "Soluciones Viales"
}, {
  "number": "4.7.2",
  "name": "Infraestructura Urbana"
}, {
  "number": "4.8.1",
  "name": "Creación de la Secretaría de Servicios Públicos"
}, {
  "number": "4.8.2",
  "name": "Alumbrado público"
}, {
  "number": "4.8.3",
  "name": "Conservación de la vía pública"
}, {
  "number": "4.8.4",
  "name": "Por un Nuevo Laredo más limpio"
}, {
  "number": "4.8.5",
  "name": "Comisión Municipal de Agua Potable y Alcantarillado (COMAPA) "
}, {
  "number": "4.8.6",
  "name": "Proyectos especiales de servicios públicos"
}, {
  "number": "5.1.1",
  "name": "Drenaje y aguas residuales"
}, {
  "number": "5.1.2",
  "name": "Arroyos y canales"
}, {
  "number": "5.1.3",
  "name": "Programa de reforestación Urbana "
}, {
  "number": "5.2.1",
  "name": "Programa de Cultura de Limpieza"
}, {
  "number": "5.3.1",
  "name": "Programa de Cultura ecológica "
}, {
  "number": "5.3.2",
  "name": "Normatividad Ambiental"
}, {
  "number": "6.1.1",
  "name": "Apoyo materiales a las escuelas "
}, {
  "number": "6.1.2",
  "name": "Uniformes, Útiles y Becas Municipales"
}, {
  "number": "6.1.3",
  "name": "Apoyo con recursos humanos a distintas escuelas "
}, {
  "number": "6.1.4",
  "name": "Seguimiento de Programas y Certámenes"
}, {
  "number": "6.2.1",
  "name": "Gestión Social"
}, {
  "number": "6.2.2",
  "name": "Estadística y Acción Social"
}, {
  "number": "6.2.3",
  "name": "Programas Federales"
}, {
  "number": "6.2.4",
  "name": "Centros Sociales y Culturales"
}, {
  "number": "6.2.5",
  "name": "Sistema de Desarrollo Integral de la Familia en Nuevo Laredo "
}, {
  "number": "6.3.1",
  "name": "Festivales"
}, {
  "number": "6.3.2",
  "name": "Talleres de arte y cultura"
}, {
  "number": "6.3.3",
  "name": "Espacios culturales"
}, {
  "number": "6.3.4",
  "name": "Exposiciones"
}, {
  "number": "6.3.5",
  "name": "Eventos"
}, {
  "number": "6.3.6",
  "name": "Arte Cotidiano "
}, {
  "number": "6.3.7",
  "name": "Instituto de Cultura (INDECULT)"
}, {
  "number": "6.4.1",
  "name": "Programa para elevar el nivel deportivo"
}, {
  "number": "6.4.2",
  "name": "Fomento Deportivo "
}, {
  "number": "6.4.3",
  "name": "Capacitación y medicina del Deporte."
}, {
  "number": "6.4.4",
  "name": "Apoyos al Deporte"
}, {
  "number": "6.4.5",
  "name": "Instituto del Deporte"
}, {
  "number": "6.5.1",
  "name": "Atención Médica Municipal"
}, {
  "number": "6.6.1",
  "name": "Incorporar la igualdad de género y empoderamiento de la mujer"
}, {
  "number": "6.6.2",
  "name": "Atención integral a las mujeres que viven violencia"
}, {
  "number": "6.6.3",
  "name": "Combate a la discriminación y exclusión de las mujeres"
}, {
  "number": "6.7.1",
  "name": "Creación del programa PAC"
}, {
  "number": "6.7.2",
  "name": "Implementación "
}, {
  "number": "6.8.1",
  "name": "Actividades Culturales y de capacitación "
}, {
  "number": "6.8.2",
  "name": "Mantenimiento General"
}, {
  "number": "6.8.3",
  "name": "Seguridad y salud de las especies "
}, {
  "number": "6.8.4",
  "name": "Nuevos Proyectos"
}]