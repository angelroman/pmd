<div class="container-full">
    <div class="container">
        <div class="titulo center-align"></div>
        <div class="row">
            <div class="col s12 m12 l6">
                <div class="objetivos">
                    <div class="title-objective"></div>
                    <ul class="ul-objetivos"></ul>
                </div>
            </div>
            <div class="col s12 m12 l6">
                <div class="estrategias">
                    <div class="title-strategy"></div>
                    <ul class="ul-estrategias"></ul>
                </div>
            </div>
        </div>
    </div>
</div>