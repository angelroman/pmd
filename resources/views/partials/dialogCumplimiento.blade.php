<div id="dialogAccomplishment">
    <div class="animated dialog-background">
    </div>
    <div class="animated dialog-wrapper">
        <div class="animated dialog">
            <div class="dialog-btn-close circle"><i class="fa fa-times"></i></div>
            <div class="dialog-content">
                <div class="dialog-title">
                    <h2>Titulo de la linea de accion</h2>
                </div>
                <div class="dialog-body">
                    <div class="container">
                        <div class="row">
                            <div class="col s12">
                                <div id="line-check">
                                    <i class="fa fa-check fa-5x animated flipInY infinite"></i>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l6" id="line-years">
                                <ul class="collection"></ul>
                            </div>
                            <div class="col s12 m6 l6" id="line-reponsable">
                                <ul class="collection"></ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dialog-footer"></div>
            </div>
        </div>
    </div>
</div>