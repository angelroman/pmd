@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Inicio de sessi&oacute;n</div>
                    <div class="panel-body">
                        <form class="form-horizontal" id="frmLogin" role="form" method="POST" {{--action="{{ url('/login') }}"--}}>
                            {{ csrf_field() }}

                            <div class="row">
                                <div class="input-field col s6{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}">
                                    <label for="email">Email</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s6{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input id="password" type="password" class="validate" name="password">
                                    <label for="password">Contrase&ntilde;a</label>
                                </div>
                            </div>


                            {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Correo</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>--}}

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        {{--<div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember"> Rordarme
                                            </label>
                                        </div>--}}
                                        <p>
                                            <input type="checkbox" id="remember" name="remember" />
                                            <label for="remember">Recordarme</label>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-btn fa-sign-in"></i> Entrar
                                        </button>
                                        <a class="btn-link" href="{{ url('/password/reset') }}">¿Olvidaste tu contrase&ntilde;a?</a>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                @if ($errors->has('email'))
                                    <span class="help-block card-panel red darken-1">
                                        <strong class="text error">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                @if ($errors->has('password'))
                                    <span class="help-block card-panel red darken-1">
                                        <strong class="text error">{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script type="text/javascript" src="../assets/js/login.js"></script>
    <script type="text/javascript" src="../assets/js/User.js"></script>
@endsection