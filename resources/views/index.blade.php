<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Seguiminento del Plan Municipal de Desarrollo 2013-2016</title>
    <link rel="stylesheet" href="{{ asset('assets/css/hello.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/animate.css') }}" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/materialize.min.css') }}" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}" media="screen,projection"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>

{{--<div class="fixed-action-btn click-to-toggle" style="bottom: 45px; left: 24px;">
    <a class="btn-floating btn-large waves-effect waves-light blue">
        <i class="fa fa-compass"></i>
    </a>
    <ul style="bottom: 45px; left: 24px;">
        <li><a class="btn-floating blue"><i class="fa fa-unlock-alt"></i></a></li>
        <li><a class="btn-floating blue"><i class="fa fa-unlock-alt"></i></a></li>
        <li><a class="btn-floating blue"><i class="fa fa-unlock-alt"></i></a></li>
        <li><a class="btn-floating blue"><i class="fa fa-unlock-alt"></i></a></li>
        <li><a class="btn-floating blue"><i class="fa fa-unlock-alt"></i></a></li>
        <li><a class="btn-floating blue"><i class="fa fa-unlock-alt"></i></a></li>
    </ul>
</div>--}}
{{--<div class="fixed-action-btn" id="btnShowBar" style="top: 35px; left: 35px;">
    <a class="btn-floating btn-large waves-effect waves-light blue">
        <i class="fa fa-compass"></i>
    </a>
    <h6 style="display: inline;">Ejes rectores</h6>
</div>--}}
    @include('partials.dialogCumplimiento')
    <div id="background"></div>


    <div id="fixed-container">
        <div class="fixed-element valign-wrapper" id="element-1"><h5 class="valign center-align">I</h5></div>
        <div class="fixed-element valign-wrapper" id="element-2"><h5 class="valign center-align">II</h5></div>
        <div class="fixed-element valign-wrapper" id="element-3"><h5 class="valign center-align">III</h5></div>
        <div class="fixed-element valign-wrapper" id="element-4"><h5 class="valign center-align">IV</h5></div>
        <div class="fixed-element valign-wrapper" id="element-5"><h5 class="valign center-align">V</h5></div>
        <div class="fixed-element valign-wrapper" id="element-6"><h5 class="valign center-align">VI</h5></div>
    </div>

    @include('partials.modalLines')

    <div class="container-full" id="container-page">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l6" id="container-seguimiento">
                    <h2 class="titulo-seguimiento">Seguimiento del <span>Plan Municipal de Desarrollo</span><br/>2013-2016</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, aut, iusto, explicabo dignissimos commodi facilis ratione velit saepe incidunt asperiores quasi qui dolore minus culpa amet odit labore provident obcaecati.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore, aut deserunt nisi perspiciatis laboriosam cupiditate expedita perferendis neque rerum architecto tempora voluptatem. Eos, sint, debitis iusto adipisci ullam mollitia labore.</p>
                </div>
                <div class="col s12 m12 l3 tarjeta-col">
                    <div class="row tarjeta">
                        <div>
                            <div class="row center-align">
                                <h2><span class="timerCard">6</span></h2>
                                <div class="tarjeta-titulo center-align">
                                    <h4>Ejes rectores</h4>
                                </div>
                                <i class="fa fa-compass fa-5x valign center-align tarjeta-icon" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row tarjeta">
                        <div>
                            <div class="row center-align">
                                <h2><span class="timerCard">118</span></h2>
                                <div class="tarjeta-titulo center-align">
                                    <h4>Estrateg&iacute;as</h4>
                                </div>
                                <i class="fa fa-lightbulb-o fa-5x valign center-align tarjeta-icon" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col s12 m12 l3 tarjeta-col">
                    <div class="row tarjeta">
                        <div>
                            <div class="row center-align">
                                <h2><span class="timerCard">36</span></h2>
                                <div class="tarjeta-titulo center-align">
                                    <h4>Objetivos</h4>
                                </div>
                                <i class="fa fa-flag-o fa-5x valign center-align tarjeta-icon" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row tarjeta">
                        <div>
                            <div class="row center-align">
                                <h2><span class="timerCard">368</span></h2>
                                <div class="tarjeta-titulo center-align">
                                    <h4>L&iacute;neas de acci&oacute;n</h4>
                                </div>
                                <i class="fa fa-check-square-o fa-5x valign center-align tarjeta-icon" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="arrow-down animated infinite pulse">
        <i class="fa fa-angle-down fa-4x" aria-hidden="true"></i>
    </div>

    <div class="arrow-up">
        <i class="fa fa-angle-up fa-3x" aria-hidden="true"></i>
    </div>
    <section class="section scrollspy" id="eje1">
        @include('partials.axis')
    </section>
    <section class="section scrollspy" id="eje2">
        @include('partials.axis')
    </section>
    <section class="section scrollspy" id="eje3">
        @include('partials.axis')
    </section>
    <section class="section scrollspy" id="eje4">
        @include('partials.axis')
    </section>
    <section class="section scrollspy" id="eje5">
        @include('partials.axis')
    </section>
    <section class="section scrollspy" id="eje6">
        @include('partials.axis')
    </section>

    <script type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/materialize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/lib/noframework.waypoints.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.countTo.js') }}"></script>
    {{--<script src="{{ asset('assets/lib/zepto.waypoints.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('assets/js/Dialog.js') }}"></script>
    <script type="text/javascript" src="{{ asset('data/Axes.js') }}"></script>
    <script type="text/javascript" src="{{ asset('data/Objectives.js') }}"></script>
    <script type="text/javascript" src="{{ asset('data/Strategies.js') }}"></script>
    <script type="text/javascript" src="{{ asset('data/Lines.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/Axis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/Objective.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/Strategy.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/Line.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/User.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/index.js') }}"></script>
</body>
</html>