<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SecretariesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('secretaries')->insert([
            ['name' => 'ADMON'],
            ['name' => 'TESO'],
            ['name' => 'PRES'],
            ['name' => 'CONTR'],
            ['name' => 'OBRAS'],
            ['name' => 'SERVICIOS'],
            ['name' => 'PLAN'],
            ['name' => 'SOCIAL'],
            ['name' => 'SERVI'],
            ['name' => 'AYU'],
            ['name' => 'ECONO'],
            ['name' => 'SEGUR'],
            ['name' => 'BAZAN'],
            ['name' => 'DIF'],
            ['name' => 'COMAPA']
        ]);
    }
}
