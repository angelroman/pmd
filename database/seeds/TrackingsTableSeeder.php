<?php

use Illuminate\Database\Seeder;

class TrackingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$json = File::get("database/data/Tracks.json");
        $json = File::get('database/data/Tracks.json');
        $Trackings = json_decode($json);
        foreach ($Trackings as $tracking) {
            \App\Tracking::create(array(
                'accomplishment'    => $tracking->accomplishment,
                'observations'      => $tracking->observations,
                'responsable'       => $tracking->responsable,
                'line_id'           => $tracking->line_id
            ));
        }
    }
}
