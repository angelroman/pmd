<?php

use Illuminate\Database\Seeder;
use App\Year;
use App\Tracking;
class YearsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('years')->insert([
            array('nameYear' => '2014'),
            array('nameYear' => '2015')
        ]);
        $json = File::get("database/data/Trackings.json");
        $Trackings = json_decode($json);
        $t = null;
        $y = Year::where('nameYear', '2014')->first();
        foreach ($Trackings as $track) {
            $track->_2014 =  strtoupper($track->_2014);
            $t = Tracking::where('line_id', $track->line_id)->first();
            $t->years()->attach($t->id, [
                'accomplishment' => $track->_2014,
                'year_id' => $y->id
            ]);
        }
        $y = Year::where('nameYear', '2015')->first();
        foreach ($Trackings as $track) {
            $track->_2015 =  strtoupper($track->_2015);
            $t = Tracking::where('line_id', $track->line_id)->first();
            $t->years()->attach($t->id, [
                'accomplishment' => $track->_2015,
                'year_id' => $y->id
            ]);
        }
    }
}
