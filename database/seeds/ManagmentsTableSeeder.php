<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Managment;
class ManagmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('managments')->insert([
            ['name' => 'CIAC'],
            ['name' => 'SISTEMAS'],
            ['name' => 'SIS'],
            ['name' => 'INGR'],
            ['name' => 'SIAC'],
            ['name' => 'TRANSP'],
            ['name' => 'CALIDAD'],
            ['name' => 'RH'],
            ['name' => 'DDG'],
            ['name' => 'COMUN'],
            ['name' => 'MTTO'],
            ['name' => 'ADQ'],
            ['name' => 'CONTAB'],
            ['name' => 'EGRE'],
            ['name' => 'PATRIMONIO'],
            ['name' => 'EJEC'],
            ['name' => 'IMJUVE'],
            ['name' => 'DSEGU'],
            ['name' => 'TRANSITO'],
            ['name' => 'PC'],
            ['name' => 'IMPACTA'],
            ['name' => 'FOMENTO'],
            ['name' => 'EMPLEO'],
            ['name' => 'FOMEN'],
            ['name' => 'RURAL'],
            ['name' => 'TURISMO'],
            ['name' => 'ICCE'],
            ['name' => 'OPERATIVA'],
            ['name' => 'PARQUES'],
            ['name' => 'ZOO'],
            ['name' => 'PROY-JEF.VIAL'],
            ['name' => 'ETIENNE'],
            ['name' => 'JEF. INF. GEO.'],
            ['name' => 'PROY'],
            ['name' => 'SUP. OBRA'],
            ['name' => 'OBRAS'],
            ['name' => 'OBRA'],
            ['name' => 'OPER'],
            ['name' => 'SOCIAL'],
            ['name' => 'PART'],
            ['name' => 'CH'],
            ['name' => 'BAZAN'],
            ['name' => 'RELACIONES'],
            ['name' => 'TUR'],
            ['name' => 'DURB'],
            ['name' => 'IMPLADU'],
            ['name' => 'JURI-IMVISU'],
            ['name' => 'JURID'],
            ['name' => 'IMVISU'],
            ['name' => 'GTECNICA'],
            ['name' => 'MA'],
            ['name' => 'EDUCACION'],
            ['name' => 'EDUC'],
            ['name' => 'CULTURA'],
            ['name' => 'ENTIDADES'],
            ['name' => 'DEPORTES'],
            ['name' => 'SALUD'],
            ['name' => 'IMM'],
            ['name' => 'PAC']
        ]);
    }
}
