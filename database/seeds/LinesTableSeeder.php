<?php

/**
 * Created by PhpStorm.
 * User: Angel
 * Date: 14/08/2016
 * Time: 4:11
 */
use Illuminate\Database\Seeder;
use App\Line;
use App\Strategy;
class LinesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('lines')->delete();
        $json = File::get("database/data/Lines.json");
        $Lines = json_decode($json);
        $strategy_id = null;
        $strategy = null;
        foreach ($Lines as $line) {
            $strategy_id = explode('.', $line->id);
            $strategy = Strategy::where('number', ($strategy_id[0].".".$strategy_id[1].".".$strategy_id[2]))->first();
            Line::create(array(
                'number' => $line->id,
                'name' => $line->name,
                'strategy_id' => $strategy->id
            ));
        }
    }
}