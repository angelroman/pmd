<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AxesTableSeeder::class);
        $this->call(ObjectivesTableSeeder::class);
        $this->call(StrategiesTableSeeder::class);
        $this->call(LinesTableSeeder::class);
        $this->call(ManagmentsTableSeeder::class);
        $this->call(SecretariesTableSeeder::class);
        $this->call(TrackingsTableSeeder::class);
        $this->call(YearsTableSeeder::class);
        $this->call(SecretaryTrackingTableSeeder::class);
        $this->call(ManagmentTrackingSeeder::class);
    }
}
