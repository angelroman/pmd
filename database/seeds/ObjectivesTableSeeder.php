<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Objective;
use App\Axis;
class ObjectivesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('objectives')->delete();
        $json = File::get("database/data/Objectives.json");
        $Objectives = json_decode($json);
        $objective = null;
        $axis_id = null;
        $axis = null;
        foreach ($Objectives as $_objective) {
            $axe_id = explode('.', $_objective->id);
            $axis = Axis::where('number', $axe_id[0])->first();
            $objective = new Objective();
            $objective->number = $_objective->id;
            $objective->name = $_objective->name;
            $objective->axis_id = $axis->id;
            $objective->save();
            /*Objective::create(array(
                'number' => $_objective->id,
                'name' => $_objective->name,
                'axis_id' => $axe_id[0]
            ));*/
        }
    }
}
