<?php

use Illuminate\Database\Seeder;
use App\Managment;
use App\Tracking;
class ManagmentTrackingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get("database/data/Trackings.json");
        $Trackings = json_decode($json);
        $direccion = null;
        $track = null;
        foreach($Trackings as $tracking) {
            $track = Tracking::where('line_id', $tracking->line_id)->first();
            for($i = 0; $i < count($tracking->Direcciones); $i++) {
                $direccion = Managment::where('name', strtoupper($tracking->Direcciones[$i]))->first();
                $track->managments()->attach($direccion);
            }
        }
    }
}
