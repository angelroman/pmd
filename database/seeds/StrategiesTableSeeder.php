<?php

/**
 * Created by PhpStorm.
 * User: Angel
 * Date: 14/08/2016
 * Time: 4:09
 */
use Illuminate\Database\Seeder;
use App\Strategy;
use App\Objective;
class StrategiesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('strategies')->delete();
        $json = File::get("database/data/Strategies.json");
        $Startegies = json_decode($json);
        $objective_id = null;
        $objective = null;
        foreach ($Startegies as $strategy) {
            $objective_id = explode('.', $strategy->id); //1.1.1
            $objective = Objective::where('number', ($objective_id[0].".".$objective_id[1]))->first();
            Strategy::create(array(
                'number' => $strategy->id,
                'name' => $strategy->name,
                'objective_id' => $objective->id
            ));
        }
    }
}