<?php

use Illuminate\Database\Seeder;
use App\Tracking;
use App\Secretary;

class SecretaryTrackingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get("database/data/Trackings.json");
        $Trackings = json_decode($json);
        $secretaria = null;
        $t = null;
        foreach($Trackings as $tracking) {
            $t = Tracking::where('line_id', $tracking->line_id)->first();
            for($i = 0; $i < count($tracking->Secretarias); $i++) {
                $secretaria = Secretary::where('name', $tracking->Secretarias[$i])->first();
                $t->secretaries()->attach($secretaria->id);
            }
        }
    }
}
