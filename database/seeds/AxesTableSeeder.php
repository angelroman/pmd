<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Axis;
class AxesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('axes')->delete();
        $json = File::get("database/data/Axes.json");
        $Axes = json_decode($json);
        $axis = null;
        foreach ($Axes as $_axis) {
            Axis::create(array(
                'number' => $_axis->id,
                'name' => $_axis->name
            ));
        }
    }
}
