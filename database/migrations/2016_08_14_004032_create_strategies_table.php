<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStrategiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('strategies', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned(); // 1. 1.1, 1.1.1,
            $table->char('number', 6)->unique();
            $table->string('name', 100);
            $table->integer('objective_id')->unsigned();
            $table->foreign('objective_id')
                ->references('id')->on('objectives')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('strategies');
    }
}
