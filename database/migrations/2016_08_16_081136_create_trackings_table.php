<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trackings', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->enum('accomplishment', ['S', 'N', 'P', 'S/N']);
            $table->string('observations', 1000);
            $table->string('responsable', 500);
            /*$table->integer('responsable_id')->unsigned();
            $table->foreign('responsable_id')
                ->references('id')->on('responsible')
                ->onUpdate('cascade')
                ->onDelete('restrict');*/
            $table->integer('line_id')->unsigned();
            $table->foreign('line_id')
                ->references('id')->on('lines')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            //$table->primary('id');
            //$table->timestamps();
            $table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('updated_at')->nullable();
        });
        Schema::create('managment_tracking', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('managment_id')->unsigned();
            $table->foreign('managment_id')
                ->references('id')->on('managments')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->integer('tracking_id')->unsigned();
            $table->foreign('tracking_id')
                ->references('id')->on('trackings')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
        Schema::create('secretary_tracking', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('secretary_id')->unsigned();
            $table->foreign('secretary_id')
                ->references('id')->on('secretaries')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->integer('tracking_id')->unsigned();
            $table->foreign('tracking_id')
                ->references('id')->on('trackings')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
        Schema::create('tracking_year', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->enum('accomplishment', ['N', 'P', 'S', 'S/N', 'X']);
            $table->integer('year_id')->unsigned();
            $table->foreign('year_id')
                ->references('id')->on('years')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->integer('tracking_id')->unsigned();
            $table->foreign('tracking_id')
                ->references('id')->on('trackings')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('secretary_tracking');
        Schema::drop('managment_tracking');
        Schema::drop('tracking_year');
        Schema::drop('trackings');
    }
}
